from covid_api.business_object.Indicatif_region import indicatif_region
from covid_api.dao.Indicatif_region_dao import indicatif_region_dao

class indicatif_region_service:

    @staticmethod
    def get_donnee_from_db_by_num_reg(num_reg):
        """
        Récupère les données hospitalières selon le département
        :param dep: l'indicatif du département recherchée
        :type dep: int
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi
        """
        return indicatif_region_dao.find_by_num_reg(num_reg)

    @staticmethod
    def get_donnee_from_db_by_region_name(region_name):
        """
        Récupère les données hospitalières selon le sexe
        :param sexe: le sexe choisi
        :type sexe: int
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi
        """
        return indicatif_region_dao.find_by_region_name(region_name)


    @staticmethod
    def get_all_donnee_from_db():
        """
        Récupère toutes les données hospitalières de la table
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi_classes_age
        """
        return indicatif_region_dao.find_all()

    @staticmethod
    def add_donnee_to_db(donnee):
        """
        Ajoute une donnée hospitalière à la base de données
        :param donnee: la donnée à ajouter
        :type donnee: Donnees_hospi_classes_age
        :return:
        :rtype:
        """
        return indicatif_region_dao.create(donnee)

    @staticmethod
    def build_donnee(**dict_attributs):
        """
        Crée une instance de donnée hospitalière à partir d'un dictionnaire avec les bonnes clés
        :param dict_attributs: Un dictionnaire contenant les différents champs d'une donnée hospitalière
        :type dict_attributs: dict
        :return: La donnée ainsi construite
        :rtype: Donnees_hospi_classes_age
        """
        donnee = indicatif_region()
        if "num_reg" in dict_attributs: # à lier ensuite à la classe reg
            donnee.num_reg = dict_attributs["num_reg"]
        #if "dep" in dict_attributs:
            #dep = Departement.find_by_id(dict_attributs["dep"])
            #if dep:
                #donnee.dep = dep
            #pas de else : si l'indicatif est faux on ne crée pas de région
        if "region_name" in dict_attributs: # à lier ensuite à la classe cl_age90
            donnee.region_name = dict_attributs["region_name"]
        return donnee