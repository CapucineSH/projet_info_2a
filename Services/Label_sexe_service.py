from covid_api.business_object.Label_sexe import label_sexe
from covid_api.dao.Label_sexe_dao import label_sexe_dao

class label_sexe_service:

    @staticmethod
    def get_donnee_from_db_by_nom_sexe(nom_sexe):
        """
        Récupère les données reg/dep selon le numéro de dep
        :param num_dep: le num de departement à rechercher
        :type num_dep: int
        :return: une liste de données dep/reg
        :rtype: list of departements_region
        """
        return label_sexe_dao.find_by_nom_sexe(nom_sexe)

    @staticmethod
    def get_donnee_from_db_by_label_num(label_num):
        """
        Récupère les données dep/region selon le nom du région
        :param region_name: la région recherchée 
        :type region_name: str
        :return: une liste de données dep/reg
        :rtype: list of Departements_region
        """
        return label_sexe_dao.find_by_label_num(label_num)


    @staticmethod
    def get_all_donnee_from_db():
        """
        Récupère toutes les données dep/reg de la table
        :return: une liste de données dep/reg
        :rtype: list of Departements_region
        """
        return label_sexe_dao.find_all()

    @staticmethod
    def add_donnee_to_db(donnee):
        """
        Ajoute une donnée dep/reg à la base de données
        :param donnee: la donnée à ajouter
        :type donnee: Departements_region
        :return:
        :rtype:
        """
        return label_sexe_dao.create(donnee)

    @staticmethod
    def build_donnee(**dict_attributs):
        """
        Crée une instance de donnée dep/reg à partir d'un dictionnaire avec les bonnes clés
        :param dict_attributs: Un dictionnaire contenant les différents champs d'une donnée dep/reg
        :type dict_attributs: dict
        :return: La donnée ainsi construite
        :rtype: Departements_region
        """
        donnee = label_sexe()
        if "nom_sexe" in dict_attributs: # à lier ensuite à la classe reg
            donnee.nom_sexe = dict_attributs["nom_sexe"]
        #if "num_dep" in dict_attributs:
            #num_dep = Departements_region.find_by_dep(dict_attributs["num_dep"])
            #if num_dep:
                #donnee.num_dep = num_dep
            #pas de else : si l'indicatif est faux on ne crée pas de donnée
        if "label_num" in dict_attributs: # à lier ensuite à la classe cl_age90
            donnee.label_num = dict_attributs["label_num"]
        
        return donnee
