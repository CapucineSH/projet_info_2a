from covid_api.business_object.Departements_region import departements_region
from covid_api.dao.Departements_region_dao import departements_region_dao

class departements_region_service:

    @staticmethod
    def get_donnee_from_db_by_num_dep(num_dep):
        """
        Récupère les données reg/dep selon le numéro de dep
        :param num_dep: le num de departement à rechercher
        :type num_dep: int
        :return: une liste de données dep/reg
        :rtype: list of departements_region
        """
        return departements_region_dao.find_by_num_dep(num_dep)

    @staticmethod
    def get_donnee_from_db_by_region_name(region_name):
        """
        Récupère les données dep/region selon le nom du région
        :param region_name: la région recherchée 
        :type region_name: str
        :return: une liste de données dep/reg
        :rtype: list of Departements_region
        """
        return departements_region_dao.find_by_region_name(region_name)

    @staticmethod
    def get_donnee_from_db_by_dep_name(dep_name):
        """
        Récupère les données dep/region selon le nom du région
        :param region_name: la région recherchée
        :type region_name: str
        :return: une liste de données dep/reg
        :rtype: list of Departements_region
        """
        return departements_region_dao.find_by_dep_name(dep_name)

    @staticmethod
    def get_donnee_from_db_by_Population(Population):
        """
        Récupère les données dep/region selon le nom du région
        :param region_name: la région recherchée
        :type region_name: str
        :return: une liste de données dep/reg
        :rtype: list of Departements_region
        """
        return departements_region_dao.find_by_Population(Population)
    

    @staticmethod
    def get_all_donnee_from_db():
        """
        Récupère toutes les données dep/reg de la table
        :return: une liste de données dep/reg
        :rtype: list of Departements_region
        """
        return departements_region_dao.find_all()

    @staticmethod
    def add_donnee_to_db(donnee):
        """
        Ajoute une donnée dep/reg à la base de données
        :param donnee: la donnée à ajouter
        :type donnee: Departements_region
        :return:
        :rtype:
        """
        return departements_region_dao.create(donnee)

    @staticmethod
    def build_donnee(**dict_attributs):
        """
        Crée une instance de donnée dep/reg à partir d'un dictionnaire avec les bonnes clés
        :param dict_attributs: Un dictionnaire contenant les différents champs d'une donnée dep/reg
        :type dict_attributs: dict
        :return: La donnée ainsi construite
        :rtype: Departements_region
        """
        donnee = departements_region()
        if "num_dep" in dict_attributs: # à lier ensuite à la classe reg
            donnee.num_dep = dict_attributs["num_dep"]
        #if "num_dep" in dict_attributs:
            #num_dep = Departements_region.find_by_dep(dict_attributs["num_dep"])
            #if num_dep:
                #donnee.num_dep = num_dep
            #pas de else : si l'indicatif est faux on ne crée pas de donnée
        if "dep_name" in dict_attributs: # à lier ensuite à la classe cl_age90
            donnee.dep_name = dict_attributs["dep_name"]
        
        if "region_name" in dict_attributs:
            donnee.region_name = dict_attributs["region_name"]
        if "Population" in dict_attributs:
            donnee.Population = dict_attributs["Population"]
        
        

        return donnee
