from covid_api.business_object.Donnees_tests_fr import donnees_tests_fr
from covid_api.dao.Donnees_tests_fr_dao import donnees_tests_fr_dao

class donnees_tests_fr_service:

    @staticmethod
    def get_donnee_from_db_by_fr(fra):
        """"
        Récupère les données des tests selon la région
        :param reg : l'indicatif INSEE de la région recherchée
        :type reg: int
        :return: une liste de données tests
        :rtype: list of Donnees_tests_reg
        """
        return donnees_tests_fr_dao.find_by_fr(fra)

    @staticmethod
    def get_donnee_from_db_by_cl_age90(cl_age90):
        """
        Récupère les données hospitalières selon la classe d'âge
        :param cl_age90: la classe d'âge recherchée (tranches de dizaines d'années)
        :type cl_age90: int
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi_classes_age
        """
        return donnees_tests_fr_dao.find_by_cl_age90(cl_age90)

    @staticmethod
    def get_donnee_from_db_by_jour(jour):
        """
        Récupère les données hospitalières selon le jour
        :param jour: la date recherchée
        :type jour: str
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi_classes_age
        """
        return donnees_tests_fr_dao.find_by_jour(jour)

    @staticmethod
    def get_all_donnee_from_db():
        """
        Récupère toutes les données hospitalières de la table
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi_classes_age
        """
        return donnees_tests_fr_dao.find_all()

    @staticmethod
    def add_donnee_to_db(donnee):
        """
        Ajoute une donnée hospitalière à la base de données
        :param donnee: la donnée à ajouter
        :type donnee: Donnees_hospi_classes_age
        :return:
        :rtype:
        """
        return donnees_tests_fr_dao.create(donnee)

    @staticmethod
    def build_donnee(**dict_attributs):
        """
        Crée une instance de donnée hospitalière à partir d'un dictionnaire avec les bonnes clés
        :param dict_attributs: Un dictionnaire contenant les différents champs d'une donnée hospitalière
        :type dict_attributs: dict
        :return: La donnée ainsi construite
        :rtype: Donnees_hospi_classes_age
        """
        donnee = donnees_tests_fr()
        if "fra" in dict_attributs: # à lier ensuite à la classe reg
            donnee.fra = dict_attributs["fra"]
        #if "reg" in dict_attributs:
            #reg = Region.find_by_id(dict_attributs["reg"])
            #if reg:
                #donnee.reg = reg
            #pas de else : si l'indicatif est faux on ne crée pas de région
        if "jour" in dict_attributs:
            donnee.jour = dict_attributs["jour"]
        if "cl_age90" in dict_attributs: # à lier ensuite à la classe cl_age90
            donnee.cl_age90 = dict_attributs["cl_age90"]
        #if "cl_age90" in dict_attributs:
            #cl_age90 = Region.find_by_id(dict_attributs["reg"])
        if "P_f" in dict_attributs:
            donnee.P_f=dict_attributs["P_f"]
        if "P_h" in dict_attributs:
            donnee.P_h=dict_attributs["P_h"]
        if "P" in dict_attributs:
            donnee.hosp = dict_attributs["P"]
        if "T_f" in dict_attributs:
            donnee.T_f=dict_attributs["T_f"]
        if "T_h" in dict_attributs:
            donnee.T_h=dict_attributs["T_h"]
        if "T" in dict_attributs:
            donnee.T = dict_attributs["T"]


        return donnee