from covid_api.business_object.Donnees_hospi import donnees_hospi
from covid_api.dao.Donnees_hospi_dao import donnees_hospi_dao

class donnees_hospi_service:

    @staticmethod
    def get_donnee_from_db_by_dep(dep):
        """
        Récupère les données hospitalières selon le département
        :param dep: l'indicatif du département recherchée
        :type dep: int
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi
        """
        return donnees_hospi_dao.find_by_dep(dep)

    @staticmethod
    def get_donnee_from_db_by_sexe(sexe):
        """
        Récupère les données hospitalières selon le sexe
        :param sexe: le sexe choisi
        :type sexe: int
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi
        """
        return donnees_hospi_dao.find_by_sexe(sexe)

    @staticmethod
    def get_donnee_from_db_by_jour(jour):
        """
        Récupère les données hospitalières selon le jour
        :param jour: la date recherchée
        :type jour: str
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi_classes_age
        """
        return donnees_hospi_dao.find_by_jour(jour)

    @staticmethod
    def get_all_donnee_from_db():
        """
        Récupère toutes les données hospitalières de la table
        :return: une liste de données hospitalières
        :rtype: list of Donnees_hospi_classes_age
        """
        return donnees_hospi_dao.find_all()

    @staticmethod
    def add_donnee_to_db(donnee):
        """
        Ajoute une donnée hospitalière à la base de données
        :param donnee: la donnée à ajouter
        :type donnee: Donnees_hospi_classes_age
        :return:
        :rtype:
        """
        return donnees_hospi_dao.create(donnee)

    @staticmethod
    def build_donnee(dict_attributs):
        """
        Crée une instance de donnée hospitalière à partir d'un dictionnaire avec les bonnes clés
        :param dict_attributs: Un dictionnaire contenant les différents champs d'une donnée hospitalière
        :type dict_attributs: dict
        :return: La donnée ainsi construite
        :rtype: Donnees_hospi_classes_age
        """
        donnee = donnees_hospi()
        if "dep" in dict_attributs: # à lier ensuite à la classe reg
            donnee.dep = dict_attributs["dep"]
        #if "dep" in dict_attributs:
            #dep = Departement.find_by_id(dict_attributs["dep"])
            #if dep:
                #donnee.dep = dep
            #pas de else : si l'indicatif est faux on ne crée pas de région
        if "sexe" in dict_attributs: # à lier ensuite à la classe cl_age90
            donnee.sexe = dict_attributs["sexe"]
        #if "cl_age90" in dict_attributs:
            #cl_age90 = Region.find_by_id(dict_attributs["reg"])
            #if reg:
                #donnee.reg = reg
            #pas de else : si l'indicatif est faux on ne crée pas de région
        if "jour" in dict_attributs:
            donnee.jour = dict_attributs["jour"]
        if "hosp" in dict_attributs:
            donnee.hosp = dict_attributs["hosp"]
        if "rea" in dict_attributs:
            donnee.rea = dict_attributs["rea"]
        if "rad" in dict_attributs:
            donnee.rad = dict_attributs["rad"]
        if "dc" in dict_attributs:
            donnee.dc = dict_attributs["dc"]
        return donnee