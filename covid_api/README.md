# covid-api
API python pour récupérer des données sur le COVID

## Installation
Installer les librairies nécessaires avec pip

## Demarrer l API
```
python3 api.py
```

## Tester l API
```
curl -i -X GET http://localhost:9090/covid-api-1/donnees_tests?date=12
```