import psycopg2

from covid_api.dao.Abstract_dao import AbstractDao
from covid_api.business_object.Donnees_hospi_clage import donnees_hospi_clage
from covid_api.dao.pool_connection import PoolConnection


class donnees_hospi_clage_dao(AbstractDao):
    @staticmethod
    def create(donnees_hospi_clage):
        """
        Ajouter une donnée hospitalière par classe d'âge dans notre base de données.
        :param donnees_hospi_classes_age: la donnée hospitalière par classe d'âge à ajouter
        :type donnees_hospi_classes_age: AbstractDAO
        :return: la base de donnée complétée
        :rtype: list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "INSERT INTO donnees_hospi_classes_age (reg,cl_age90,jour,hosp,rea,rad,dc) VALUES (%s, %s, %s, %s, %s, %s, %s)"
                , (donnees_hospi_clage.reg
                , donnees_hospi_clage.cl_age90
                , donnees_hospi_clage.jour
                , donnees_hospi_clage.hosp
                , donnees_hospi_clage.rea
                , donnees_hospi_clage.rad
                , donnees_hospi_clage.dc))

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return donnees_hospi_clage


    @staticmethod
    def find_by_reg(reg):
        """
        Va chercher les données hospi dont on connait la région.
        :param reg : le numéro de la région de l'élément que l'on cherche
        :type reg : int
        :return les données hospitalières avec la bonne région
        :rtype list of AbstractDAO
        """

        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # Petite optimisation dans la requête SQL. On réalise une requête
            # intermédiaire pour filtrer les données que l'on veut récupérer
            # et ensuite on fait la jointure. Si dans la cas présent ce n'est
            # pas une grande optimisation, quand on manipule des Go de données
            # cela peut faire gagner du temps. De manière générale il faut
            # filtrer les données le plus tôt possible puis les traiter ensuite
            curseur.execute(
                "SELECT * FROM donnees_hospi_classes_age WHERE reg = %s", (reg))
            resultat = curseur.fetchone()
            mes_donnees = None
            # Si on a un résultat
            if resultat:
                mes_donnees = donnees_hospi_clage(
                    reg=resultat["reg"]
                    , cl_age90=resultat["cl_age90"]
                    , jour=resultat["jour"]
                    , hosp=resultat["hosp"]
                    , rea=resultat["rea"]
                    , rad=resultat["rad"]
                    , dc=resultat["dc"])
            # ou si ça marche pas
                #mes_donnees = donnees_hospi(
                #    reg=resultat[0]
                #    , cl_age90=resultat[1]
                #    , jour=resultat[2]
                #    , hosp=resultat[3]
                #    , rea=resultat[4]
                #    , rad=resultat[5]
                #    , dc=resultat[6])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees


    @staticmethod
    def find_by_cl_age90(cl_age90):
        """
        Va chercher les données hospi dont on connait la classe d'âge par dizaines d'années.
        :param cl_age90 : le numéro associé à la classe d'âge choisie des éléments que l'on cherche
        :type cl_age90 : int
        :return les données hospitalières avec la bonne classe d'âge
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM donnees_hospi_classes_age WHERE cl_age90 = %d", (cl_age90))
            resultat = curseur.fetchone()
            mes_donnees2 = None
            # Si on a un résultat
            if resultat:
                mes_donnees2 = donnees_hospi_clage(
                    reg=resultat["reg"]
                    , cl_age90=resultat["cl_age90"]
                    , jour=resultat["jour"]
                    , hosp=resultat["hosp"]
                    , rea=resultat["rea"]
                    , rad=resultat["rad"]
                    , dc=resultat["dc"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees2

    @staticmethod
    def find_by_jour(jour):
        """
        Va chercher les données hospi dont on connait le jour.
        :param jour : le jour associé aux éléments que l'on cherche
        :type jour : int
        :return les données hospitalières du bon jour
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM donnees_hospi_classes_age WHERE jour = %d", (jour))
            resultat = curseur.fetchone()
            mes_donnees3 = None
            # Si on a un résultat
            if resultat:
                mes_donnees3 = donnees_hospi_clage(
                    reg=resultat["reg"]
                    , cl_age90=resultat["cl_age90"]
                    , jour=resultat["jour"]
                    , hosp=resultat["hosp"]
                    , rea=resultat["rea"]
                    , rad=resultat["rad"]
                    , dc=resultat["dc"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees3

    @staticmethod
    def find_all():
        """
        Va chercher toutes des donnnées hospitalières en base
        :return Toutes les éléments d'une table sous forme de liste python
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM donnees_hospi_classes_age"
            )
            resultats = curseur.fetchall()
            mes_donnees_all = []
            for resultat in resultats:
                mes_donnees_all.append(
                    donnees_hospi_clage(
                        reg=resultat["reg"]
                        , cl_age90=resultat["cl_age90"]
                        , jour=resultat["jour"]
                        , hosp=resultat["hosp"]
                        , rea=resultat["rea"]
                        , rad=resultat["rad"]
                        , dc=resultat["dc"])
                )
                # ou si ça marche pas
                    #mes_donnees = donnees_hospi(
                    #    dep=resultat[0]
                    #    , sexe=resultat[1]
                    #    , jour=resultat[2]
                    #    , hosp=resultat[3]
                    #    , rea=resultat[4]
                    #    , rad=resultat[5]
                    #    , dc=resultat[6])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees_all