import psycopg2

from covid_api.dao.Abstract_dao import AbstractDao
from covid_api.business_object.Label_clage import label_clage
from covid_api.dao.pool_connection import PoolConnection

class label_clage_dao(AbstractDao):
    @staticmethod
    def create(label_clage):
        """
        Ajoute un indicateur pour le numero de region dans notre base de données.
        :param indicatif_region: l'indicateur à ajouter
        :type indicatif_region: int
        :return: la base de données complétées
        :rtype: list of AsbtractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "INSERT INTO label_clage (nom_clage, label) VALUES (%s, %s)"
                , (label_clage.nom_clage
                , label_clage.label))

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return label_clage

    @staticmethod
    def find_by_nom_clage(nom_clage):
        """
        Va chercher les données hospi dont on connait le département.
        :param dep : le numéro du département de l'élément que l'on cherche
        :type dep : int
        :return les données hospitalières avec le bon département
        :rtype list of AsbtractDAO
        """

        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # Petite optimisation dans la requête SQL. On réalise une requête
            # intermédiaire pour filtrer les données que l'on veut récupérer
            # et ensuite on fait la jointure. Si dans la cas présent ce n'est
            # pas une grande optimisation, quand on manipule des Go de données
            # cela peut faire gagner du temps. De manière générale il faut
            # filtrer les données le plus tôt possible puis les traiter ensuite
            curseur.execute(
                "SELECT * FROM label_clage WHERE nom_clage = %s", (nom_clage,))
            resultat = curseur.fetchone()
            mes_donnees = None
            # Si on a un résultat
            if resultat:
                mes_donnees = label_clage(
                    nom_clage=resultat["nom_clage"]
                    , label=resultat["label"])
            # ou si ça marche pas
                #mes_donnees = donnees_hospi(
                #    dep=resultat[0]
                #    , sexe=resultat[1]
                #    , jour=resultat[2]
                #    , hosp=resultat[3]
                #    , rea=resultat[4]
                #    , rad=resultat[5]
                #    , dc=resultat[6])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees


    @staticmethod
    def find_by_label(label):
        """
        Va chercher les données hospi dont on connait le sexe : homme = 1 ou femme = 2.
        :param sexe : le numéro associé au sexe choisi des éléments que l'on cherche
        :type sexe : int
        :return les données hospitalières avec le bon sexe
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM label_clage WHERE label = %s", (label,))
            resultat = curseur.fetchone()
            mes_donnees2 = None
            # Si on a un résultat
            if resultat:
                mes_donnees2 = label_clage(
                    nom_clage=resultat["nom_clage"]
                    , label=resultat["label"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees2

    @staticmethod
    def find_all():
        """
        Va chercher toutes des donnnées hospitalières en base
        :return Toutes les éléments d'une table sous forme de liste python
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM label_clage"
            )
            resultats = curseur.fetchall()
            mes_donnees_all = []
            for resultat in resultats:
                mes_donnees_all.append(
                    label_clage(
                        nom_clage=resultat["nom_clage"]
                        , label=resultat["label"])
                )
                # ou si ça marche pas
                    #mes_donnees = donnees_hospi(
                    #    dep=resultat[0]
                    #    , sexe=resultat[1]
                    #    , jour=resultat[2]
                    #    , hosp=resultat[3]
                    #    , rea=resultat[4]
                    #    , rad=resultat[5]
                    #    , dc=resultat[6])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees_all

