import psycopg2

from covid_api.dao.Abstract_dao import AbstractDao
from covid_api.business_object.Departements_region import departements_region
from covid_api.dao.pool_connection import PoolConnection

class departements_region_dao(AbstractDao):
    #@staticmethod
    def create(departements_region):
        """
        Ajouter une une région et un département dans notre base de données.
        :param departements_region: la region/département à ajouter
        :type departements_region: AbstractDAO
        :return: la base de données complétées
        :rtype: list of AsbtractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "INSERT INTO table_pop_dep_reg (dep_name,Population,num_dep,region_name) VALUES (%s,%s,%s,%s)"
                , (departements_region.dep_name
                , departements_region.Population
                , departements_region.num_dep
                , departements_region.region_name))

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return departements_region

    def delete(departements_region):
        """
        Supprime une donnée dep/reg de la base
        :param departements_region: la donnée à supprimer
        :return: si la suppresion a été faite
        :rtype: bool
        """
        deleted = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "DELETE FROM table_pop_dep_reg WHERE num_dep=%s;"
                , (departements_region.num_dep,))
            # attention quand vous n'avez qu'un champ il faut garder une
            # structure de tuple et donc bien mettre un virgule avec
            # rien derrière

            # on verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted




    #@staticmethod
    def find_by_num_dep(num_dep):
        """
        Va chercher le département correspondant au num donné.
        :param num_dep : le numéro du département de l'élément que l'on cherche
        :type num_dep : int
        :return les données dep/region avec le bon département
        :rtype list of AsbtractDAO
        """

        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # Petite optimisation dans la requête SQL. On réalise une requête
            # intermédiaire pour filtrer les données que l'on veut récupérer
            # et ensuite on fait la jointure. Si dans la cas présent ce n'est
            # pas une grande optimisation, quand on manipule des Go de données
            # cela peut faire gagner du temps. De manière générale il faut
            # filtrer les données le plus tôt possible puis les traiter ensuite
            curseur.execute(
                "SELECT * FROM table_pop_dep_reg WHERE num_dep = %s", (num_dep,))
            resultat = curseur.fetchone()
            mes_donnees = None
            # Si on a un résultat
            if resultat:
                mes_donnees = departements_region(
                    dep_name=resultat["dep_name"]
                    , Population=resultat["population"]
                    , num_dep=resultat["num_dep"]
                    , region_name=resultat["region_name"])

            # ou si ça marche pas
                #mes_donnees = departements_region(
                #    num_dep=resultat[0]
                #    , dep_name=resultat[1]
                #    , region_name=resultat[2])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees


    @staticmethod
    def find_by_region_name(region_name):
        """
        Va chercher les données region/dep dont on connait le nom de la région
        :param region_name : le nom du région qu'on cherche
        :type region_name : str
        :return les données reg/dep avec le bon nom du région
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM table_pop_dep_reg WHERE region_name = %s", (region_name,))
            resultat = curseur.fetchone()
            mes_donnees2 = None
            # Si on a un résultat
            print(resultat)
            if resultat:
                mes_donnees2 = departements_region(dep_name=resultat["dep_name"]
                    , Population=resultat['population']
                    , num_dep=resultat["num_dep"]
                    , region_name=resultat["region_name"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        print(resultat)
        return mes_donnees2


    @staticmethod
    def find_by_dep_name(dep_name):
        """
        Va chercher les données regio/dep dont on connait le nom du région
        :param region_name : le nom du région qu'on cherche
        :type region_name : str
        :return les données reg/dep avec le bon nom du résgion
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM table_pop_dep_reg WHERE dep_name = %s", (dep_name,))
            resultat = curseur.fetchone()
            mes_donnees3 = None
            # Si on a un résultat
            if resultat:
                mes_donnees3 = departements_region(dep_name=resultat["dep_name"]
                    , Population=resultat['population']
                    , num_dep=resultat["num_dep"]
                    , region_name=resultat["region_name"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees3


    @staticmethod
    def find_by_Population(Population):
        """
        Va chercher les données region/dep dont on connait la population
        :param population : population de la région/departement
        :type region_name : int
        :return les données reg/dep avec le bon nombre de population
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM table_pop_dep_reg WHERE Population = %s", (Population,))
            resultat = curseur.fetchone()
            mes_donnees4 = None
            # Si on a un résultat
            if resultat:
                mes_donnees4 = departements_region(dep_name=resultat["dep_name"]
                    , Population=resultat['population']
                    , num_dep=resultat["num_dep"]
                    , region_name=resultat["region_name"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees4

    @staticmethod
    def find_all():
        """
        Va chercher toutes des donnnées region/dep en base
        :return Toutes les éléments d'une table sous forme de liste python
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM table_pop_dep_reg")
            resultats = curseur.fetchall()
            mes_donnees_all = []
            for resultat in resultats:
                mes_donnees_all.append(departements_region(dep_name=resultat["dep_name"]
                    , Population=resultat['population']
                    , num_dep=resultat["num_dep"]
                    , region_name=resultat["region_name"])
                )
                # ou si ça marche pas
                    #mes_donnees = departements_region(
                    #    num_dep=resultat[0]
                    #    , dep_name=resultat[1]
                    #    , region_name=resultat[2])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees_all



