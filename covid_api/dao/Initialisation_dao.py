import psycopg2

from covid_api.dao.Abstract_dao import AbstractDao
from covid_api.dao.pool_connection import PoolConnection

class initialisation_dao(AbstractDao):
    @staticmethod
    def create():
        """
        fonction qui créé une table vide
        :param donnees_hospi: la données hospitalières à ajouter
        :type donnees_hospi: AbstractDAO
        :return: la base de données complétées
        :rtype: list of AsbtractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute("""
            DROP table IF EXISTS label_sexe ;
            DROP table IF EXISTS label_clage ;
            DROP table IF EXISTS table_pop_dep_reg ;
            DROP table IF EXISTS donnees_hospi ;
            DROP table IF EXISTS donnees_hospi_classes_age ;
            DROP table IF EXISTS donnees_tests_dep ;
            DROP table IF EXISTS indicatif_region ;
            DROP table IF EXISTS donnees_tests_reg ;
            DROP table IF EXISTS donnees_tests_fr ;
            
            create table label_sexe (
            nom_sexe varchar(20),
            label_num integer,
            CONSTRAINT PK_num_sexe PRIMARY KEY (label_num)
            );
            
            create table label_clage (
            nom_clage varchar(20),
            label integer,
            CONSTRAINT PK_label_clage PRIMARY KEY (label)
            );
            
            create table table_pop_dep_reg (
            dep_name varchar(50),
            Population varchar(50),
            num_dep varchar(3) NOT NULL,
            region_name varchar(50),
            CONSTRAINT PK_table_pop_dep_reg PRIMARY KEY (num_dep)
            );

            create table donnees_hospi(
            dep varchar(3) NOT NULL,
            sexe integer NOT NULL,
            jour date NOT NULL,
            hosp integer ,
            rea integer,
            rad integer,
            dc integer,
            CONSTRAINT PK_donnees_hospi PRIMARY KEY (dep,jour,sexe)
            ) ;

            create table donnees_hospi_classes_age(
            reg integer not null,
            cl_age90 integer not null,
            jour date not null ,
            hosp integer ,
            rea integer,
            rad integer,
            dc integer,
            CONSTRAINT PK_donnees_hospi_classes_age PRIMARY KEY (reg,jour,cl_age90)
            ) ;

            create table donnees_tests_dep(
            dep varchar(3) not null,
            jour date not null,
            P integer,
            T integer,
            cl_age90 integer not null,
            CONSTRAINT PK_donnees_tests_dep PRIMARY KEY (dep, jour, cl_age90)
            ) ;
            
            create table donnees_tests_reg(
            reg varchar(2) not null,
            jour date not null,
            P_f integer,
            P_h integer,
            P integer,
            T_f integer,
            T_h integer,
            T integer,
            cl_age90 integer not null,
            CONSTRAINT PK_donnees_tests_reg PRIMARY KEY (reg, jour, cl_age90)
            ) ;

            create table indicatif_region(
            num_reg integer not null,
            region_name varchar(50) not null ,
            CONSTRAINT PK_indicatif_region PRIMARY KEY (num_reg,region_name)
            ) ;
            
            create table donnees_tests_fr(
            fra varchar(2) not null,
            jour date not null,
            P_f integer,
            P_h integer,
            P integer,
            T_f integer,
            T_h integer,
            T integer,
            cl_age90 integer not null,
            CONSTRAINT PK_donnees_tests_fr PRIMARY KEY (fra, jour, cl_age90)
            ) ;
            
            """)

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return None

    @staticmethod
    def find_by_dep(dep):
        return 'null'


    @staticmethod
    def find_by_sexe(sexe):
        return 'null'

    @staticmethod
    def find_by_jour(jour):
        return 'null'

    @staticmethod
    def find_all():
        return 'null'

