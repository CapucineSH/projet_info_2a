import psycopg2

from covid_api.dao.Abstract_dao import AbstractDao
from covid_api.business_object.Indicatif_region import indicatif_region
from covid_api.dao.pool_connection import PoolConnection

class indicatif_region_dao(AbstractDao):
    @staticmethod
    def create(indicatif_region):
        """
        Ajoute un indicateur pour le numero de region dans notre base de données.
        :param indicatif_region: l'indicateur à ajouter
        :type indicatif_region: int
        :return: la base de données complétées
        :rtype: list of AsbtractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "INSERT INTO indicatif_region (num_reg, region_name) VALUES (%s, %s)"
                , (indicatif_region.num_reg
                , indicatif_region.region_name))

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return indicatif_region

    @staticmethod
    def find_by_num_reg(num_reg):
        """
        Va chercher les régions dont on connait le numéro.
        :param dep : le numéro de la région que l'on cherche
        :type dep : int
        :return la région avec le bon numéro
        :rtype list of AbstractDAO
        """

        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # Petite optimisation dans la requête SQL. On réalise une requête
            # intermédiaire pour filtrer les données que l'on veut récupérer
            # et ensuite on fait la jointure. Si dans la cas présent ce n'est
            # pas une grande optimisation, quand on manipule des Go de données
            # cela peut faire gagner du temps. De manière générale il faut
            # filtrer les données le plus tôt possible puis les traiter ensuite
            curseur.execute(
                "SELECT * FROM indicatif_region WHERE num_reg = %s", (num_reg,))
            resultat = curseur.fetchone()
            mes_donnees = None
            # Si on a un résultat
            if resultat:
                mes_donnees = indicatif_region(
                    num_reg=resultat["num_reg"]
                    , region_name=resultat["region_name"])
            # ou si ça marche pas
                #mes_donnees = donnees_hospi(
                #    dep=resultat[0]
                #    , sexe=resultat[1]
                #    , jour=resultat[2]
                #    , hosp=resultat[3]
                #    , rea=resultat[4]
                #    , rad=resultat[5]
                #    , dc=resultat[6])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees


    @staticmethod
    def find_by_region_name(region_name):
        """
        Va chercher la région dont on connait le nom.
        :param region_name : le le nom de la région que l'on cherche
        :type sexe : str
        :return la région avec le bon nom
        :rtype list of indicatif_region
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM indicatif_region WHERE region_name = %s", (region_name,))
            resultat = curseur.fetchone()
            mes_donnees2 = None
            # Si on a un résultat
            if resultat:
                mes_donnees2 = indicatif_region(
                    num_reg=resultat["num_reg"]
                    , region_name=resultat["region_name"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees2

    @staticmethod
    def find_all():
        """
        Va chercher toutes les régions en base
        :return Toutes les éléments d'une table sous forme de liste python
        :rtype list of indicatif_region
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM indicatif_region"
            )
            resultats = curseur.fetchall()
            mes_donnees_all = []
            for resultat in resultats:
                mes_donnees_all.append(
                    indicatif_region(
                        num_reg=resultat["num_reg"]
                        , region_name=resultat["region_name"])
                )
                # ou si ça marche pas
                    #mes_donnees = donnees_hospi(
                    #    dep=resultat[0]
                    #    , sexe=resultat[1]
                    #    , jour=resultat[2]
                    #    , hosp=resultat[3]
                    #    , rea=resultat[4]
                    #    , rad=resultat[5]
                    #    , dc=resultat[6])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees_all

