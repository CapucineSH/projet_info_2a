import psycopg2

from covid_api.dao.Abstract_dao import AbstractDao
from covid_api.business_object.Donnees_hospi import donnees_hospi
from covid_api.dao.pool_connection import PoolConnection

format = '%Y-%m-%d'

class donnees_hospi_dao(AbstractDao):
    @staticmethod
    def create(donnees_hospi):
        """
        Ajouter une données hospitalières dans notre base de données.
        :param donnees_hospi: la données hospitalières à ajouter
        :type donnees_hospi: AbstractDAO
        :return: la base de données complétées
        :rtype: list of AsbtractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "INSERT INTO donnees_hospi (dep,sexe,jour,hosp,rea,rad,dc) VALUES (%s, %s, %s, %s, %s, %s, %s)"
                , (donnees_hospi.dep
                , donnees_hospi.sexe
                , donnees_hospi.jour
                , donnees_hospi.hosp
                , donnees_hospi.rea
                , donnees_hospi.rad
                , donnees_hospi.dc))

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return donnees_hospi

    @staticmethod
    def find_by_dep(dep):
        """
        Va chercher les données hospi dont on connait le département.
        :param dep : le numéro du département de l'élément que l'on cherche
        :type dep : int
        :return les données hospitalières avec le bon département
        :rtype list of AsbtractDAO
        """

        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # Petite optimisation dans la requête SQL. On réalise une requête
            # intermédiaire pour filtrer les données que l'on veut récupérer
            # et ensuite on fait la jointure. Si dans la cas présent ce n'est
            # pas une grande optimisation, quand on manipule des Go de données
            # cela peut faire gagner du temps. De manière générale il faut
            # filtrer les données le plus tôt possible puis les traiter ensuite
            curseur.execute(
                "SELECT * FROM donnees_hospi WHERE dep = %s", (dep))
            resultat = curseur.fetchone()
            mes_donnees = None
            # Si on a un résultat
            if resultat:
                mes_donnees = donnees_hospi(
                    dep=resultat["dep"]
                    , sexe=resultat["sexe"]
                    , jour=resultat["jour"]
                    , hosp=resultat["hosp"]
                    , rea=resultat["rea"]
                    , rad=resultat["rad"]
                    , dc=resultat["dc"])
            # ou si ça marche pas
                #mes_donnees = donnees_hospi(
                #    dep=resultat[0]
                #    , sexe=resultat[1]
                #    , jour=resultat[2]
                #    , hosp=resultat[3]
                #    , rea=resultat[4]
                #    , rad=resultat[5]
                #    , dc=resultat[6])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees


    @staticmethod
    def find_by_sexe(sexe):
        """
        Va chercher les données hospi dont on connait le sexe : homme = 1 ou femme = 2.
        :param sexe : le numéro associé au sexe choisi des éléments que l'on cherche
        :type sexe : int
        :return les données hospitalières avec le bon sexe
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM donnees_hospi WHERE sexe = %s", (sexe))
            resultat = curseur.fetchone()
            mes_donnees2 = None
            # Si on a un résultat
            if resultat:
                mes_donnees2 = donnees_hospi(
                    dep=resultat["dep"]
                    , sexe=resultat["sexe"]
                    , jour=resultat["jour"]
                    , hosp=resultat["hosp"]
                    , rea=resultat["rea"]
                    , rad=resultat["rad"]
                    , dc=resultat["dc"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees2

    @staticmethod
    def find_by_jour(jour):
        """
        Va chercher les données hospi dont on connait le jour.
        :param jour : le jour associé aux éléments que l'on cherche
        :type jour : string
        :return les données hospitalières du bon jour
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM donnees_hospi WHERE jour = %s", (jour))
            resultat = curseur.fetchone()
            mes_donnees3 = None
            # Si on a un résultat
            if resultat:
                mes_donnees3 = donnees_hospi(
                    dep=resultat["dep"]
                    , sexe=resultat["sexe"]
                    , jour=resultat["jour"]
                    , hosp=resultat["hosp"]
                    , rea=resultat["rea"]
                    , rad=resultat["rad"]
                    , dc=resultat["dc"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees3

    @staticmethod
    def find_all():
        """
        Va chercher toutes des donnnées hospitalières en base
        :return Toutes les éléments d'une table sous forme de liste python 
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM donnees_hospi"
            )
            resultats = curseur.fetchall()
            mes_donnees_all = []
            for resultat in resultats:
                mes_donnees_all.append(
                    donnees_hospi(
                        dep=resultat["dep"]
                        , sexe=resultat["sexe"]
                        , jour=resultat["jour"]
                        , hosp=resultat["hosp"]
                        , rea=resultat["rea"]
                        , rad=resultat["rad"]
                        , dc=resultat["dc"])
                )
                # ou si ça marche pas
                    #mes_donnees = donnees_hospi(
                    #    dep=resultat[0]
                    #    , sexe=resultat[1]
                    #    , jour=resultat[2]
                    #    , hosp=resultat[3]
                    #    , rea=resultat[4]
                    #    , rad=resultat[5]
                    #    , dc=resultat[6])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees_all

