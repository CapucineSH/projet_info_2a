import psycopg2

from covid_api.dao.Abstract_dao import AbstractDao
from covid_api.business_object.Donnees_tests_reg import donnees_tests_reg
from covid_api.dao.pool_connection import PoolConnection
from view.abstract_view import Abstract_view

class donnees_tests_reg_dao(AbstractDao):
    @staticmethod
    def create(donnees_tests_reg):
        """
        Ajouter une donnée des tests liée aux régions dans notre base de données.
        :param donnees_tests_reg: la donnée des test à ajouter
        :type donnees_tests_reg: AbstractDAO
        :return: la base de donnée complétée
        :rtype: list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "INSERT INTO donnees_tests_reg (reg,jour,P_f,P_h,P,T_f,T_h,T,cl_age90) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                , (donnees_tests_reg.reg
                , donnees_tests_reg.jour
                , donnees_tests_reg.P_f
                , donnees_tests_reg.P_h
                , donnees_tests_reg.P
                , donnees_tests_reg.T
                , donnees_tests_reg.T_f
                , donnees_tests_reg.T_h
                , donnees_tests_reg.cl_age90
                ))


            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return donnees_tests_reg



    @staticmethod
    def find_by_reg(reg):
        """
        Va chercher les données tests dont on connait la région.
        :param reg : le numéro INSEE de la région de l'élément que l'on cherche
        :type reg : int
        :return les données tests avec la bonne région
        :rtype list of AsbtractDAO
        """

        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # Petite optimisation dans la requête SQL. On réalise une requête
            # intermédiaire pour filtrer les données que l'on veut récupérer
            # et ensuite on fait la jointure. Si dans la cas présent ce n'est
            # pas une grande optimisation, quand on manipule des Go de données
            # cela peut faire gagner du temps. De manière générale il faut
            # filtrer les données le plus tôt possible puis les traiter ensuite
            curseur.execute(
                "SELECT * FROM donnees_tests_reg WHERE reg = %s", (reg))
            resultat = curseur.fetchone()
            mes_donnees = None
            # Si on a un résultat
            if resultat:
                mes_donnees = donnees_tests_reg(
                    reg=resultat["reg"]
                    , jour=resultat["jour"]
                    , P_f=resultat["P_f"]
                    , P_h=resultat["P_h"]
                    , P=resultat["P"]
                    , T_f=resultat["T_f"]
                    , T_h=resultat["T_h"]
                    , T=resultat["T"]
                    , cl_age90=resultat["cl_age90"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees

    def find_by_jour(jour):
        """
        Va chercher les données des tests dont on connait le jour.
        :param jour : le jour associé aux éléments que l'on cherche
        :type jour : string
        :return les données tests du bon jour
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM donnees_tests_reg WHERE jour = %s", (jour))
            resultat = curseur.fetchone()
            mes_donnees3 = None
            # Si on a un résultat
            if resultat:
                mes_donnees3 = donnees_tests_reg(
                    reg=resultat["reg"]
                    , jour=resultat["jour"]
                    , P_f=resultat["P_f"]
                    , P_h=resultat["P_h"]
                    , P=resultat["P"]
                    , T_f=resultat["T_f"]
                    , T_h=resultat["T_h"]
                    , T=resultat["T"]
                    , cl_age90=resultat["cl_age90"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees3


    def find_by_cl_age90(cl_age90):
        """
        Va chercher les données des tests dont on connait la classe d'âge par dizaines d'années.
        :param cl_age90 : le numéro associé à la classe d'âge choisie des éléments que l'on cherche
        :type cl_age90 : int
        :return les données tests avec la bonne classe d'âge
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM donnees_tests_reg WHERE cl_age90 = %s", (cl_age90))
            resultat = curseur.fetchone()
            mes_donnees2 = None
            # Si on a un résultat
            if resultat:
                mes_donnees2 = donnees_tests_reg(
                    reg=resultat["reg"]
                    , jour=resultat["jour"]
                    , P_f=resultat["P_f"]
                    , P_h=resultat["P_h"]
                    , P=resultat["P"]
                    , T_f=resultat["T_f"]
                    , T_h=resultat["T_h"]
                    , T=resultat["T"]
                    , cl_age90=resultat["cl_age90"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees2


    @staticmethod
    def find_all():
        """
        Va chercher toutes des donnnées des tests (indexé par région) en base
        :return Toutes les éléments d'une table sous forme de liste python
        :rtype list of AbstractDAO
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * FROM donnees_tests_reg"
            )
            resultats = curseur.fetchall()
            mes_donnees_all = []
            for resultat in resultats:
                mes_donnees_all.append(
                    donnees_tests_reg(
                        reg=resultat["reg"]
                        , jour=resultat["jour"]
                        , P_f=resultat["P_f"]
                        , P_h=resultat["P_h"]
                        , P=resultat["P"]
                        , T_f=resultat["T_f"]
                        , T_h=resultat["T_h"]
                        , T=resultat["T"]
                        , cl_age90=resultat["cl_age90"])
                )
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return mes_donnees_all
