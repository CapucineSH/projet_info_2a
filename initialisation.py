import csv

from covid_api.business_object.Donnees_hospi import donnees_hospi
from covid_api.business_object.Departements_region import departements_region
from covid_api.business_object.Donnees_hospi_clage import donnees_hospi_clage
from covid_api.business_object.Donnees_tests_dep import donnees_tests_dep
from covid_api.business_object.Indicatif_region import indicatif_region
from covid_api.business_object.Donnees_tests_reg import donnees_tests_reg
from covid_api.business_object.Label_clage import label_clage
from covid_api.business_object.Label_sexe import label_sexe
from covid_api.business_object.Donnees_tests_fr import donnees_tests_fr

from covid_api.dao.Departements_region_dao import departements_region_dao
from covid_api.dao.Donnees_hospi_dao import donnees_hospi_dao
from covid_api.dao.Donnees_hospi_clage_dao import donnees_hospi_clage_dao
from covid_api.dao.Donnees_tests_dep_dao import donnees_tests_dep_dao
from covid_api.dao.Indicatif_region_dao import indicatif_region_dao
from covid_api.dao.Initialisation_dao import initialisation_dao
from covid_api.dao.Donnees_tests_reg_dao import donnees_tests_reg_dao
from covid_api.dao.Label_clage_dao import label_clage_dao
from covid_api.dao.Label_sexe_dao import label_sexe_dao
from covid_api.dao.Donnees_tests_fr_dao import donnees_tests_fr_dao

if __name__ == "__main__":

    initialisation_dao.create()
    print("DEBUT")

    with open('covid_api/csv/label_sexe.csv', 'r') as f:
        reader=csv.DictReader(f)
        for row in reader :
            nom_sexe=row['nom_sexe']
            label_num=row['label_num']
            mon_label_sexe=label_sexe(nom_sexe,label_num)
            label_sexe_dao.create(mon_label_sexe)
    print("Fin0.1")

    with open('covid_api/csv/label_clage.csv', 'r') as f:
        reader=csv.DictReader(f)
        for row in reader :
            nom_clage=row['nom_clage']
            label=row['label']
            mon_label_clage=label_clage(nom_clage,label)
            label_clage_dao.create(mon_label_clage)
    print("Fin0")

    with open('covid_api/csv/indicatif_region.csv', 'r') as f:
        reader=csv.DictReader(f)
        for row in reader :
            num_region=row['reg']
            region_name=row['ncc']
            mon_indicatif_region=indicatif_region(num_region, region_name)
            indicatif_region_dao.create(mon_indicatif_region)
    print("Fin1")

    with open('covid_api/csv/table_pop_dep_reg.csv', 'r') as f:
        # we don't need the `csv` module.
        reader=csv.DictReader(f)  # lit ligne à ligne le csv
        for row in reader :
            nom_departement = row['dep_name']
            Population = row['Population']
            numero_departement = row['num_dep']
            region=row['region_name']
            mon_departement_region=departements_region(nom_departement, Population, numero_departement, region)
            departements_region_dao.create(mon_departement_region)
    print("Fin2")

    with open('covid_api/csv/donnees_tests_dep.csv', 'r') as f:
        # we don't need the `csv` module.
        reader=csv.DictReader(f, delimiter=';')  # lit ligne à ligne le csv
        for row in reader :
            code_dep = row['dep']
            jour=row['jour']
            positif=row['P']
            total=row['T']
            classe_age=row['cl_age90']
            ma_donnees_test_dep=donnees_tests_dep(code_dep, jour, positif, total, classe_age)
            donnees_tests_dep_dao.create(ma_donnees_test_dep)
    print("Fin3")

    with open('covid_api/csv/donnees_tests_reg.csv', 'r') as f:
        # we don't need the `csv` module.
        reader=csv.DictReader(f, delimiter=';')  # lit ligne à ligne le csv
        for row in reader :
            code_reg = row['reg']
            jour=row['jour']
            positif_femme=row["P_f"]
            positif_homme=row['P_h']
            positif=row['P']
            total_femme = row['T_f']
            total_homme = row['T_h']
            total=row['T']
            classe_age=row['cl_age90']
            ma_donnees_test_reg=donnees_tests_reg(code_reg, jour, positif_femme, positif_homme, positif, total_femme, total_homme, total, classe_age)
            donnees_tests_reg_dao.create(ma_donnees_test_reg)
    print("Fin4")

    with open('covid_api/csv/donnees_hospi.csv', 'r') as f:
        # we don't need the `csv` module.
        reader=csv.DictReader(f, quotechar='"', delimiter=';')  # lit ligne à ligne le csv
        for row in reader :
            numero_departement = row['dep']
            sexe=row['sexe']
            jour=row['jour']
            hosp=row['hosp']
            rea=row['rea']
            rad=row['rad']
            dc=row['dc']
            ma_donnees_hospi=donnees_hospi(numero_departement,sexe,jour,hosp,rea,rad,dc)
            donnees_hospi_dao.create(ma_donnees_hospi)
    print("Fin5")

    with open('covid_api/csv/donnees_hospi_classes_age.csv', 'r') as f:
        # we don't need the `csv` module.
        reader=csv.DictReader(f, quotechar='"', delimiter=';')  # lit ligne à ligne le csv
        for row in reader :
            numero_region = row['reg']
            classe_age=row['cl_age90']
            jour=row['jour']
            hosp=row['hosp']
            rea=row['rea']
            rad=row['rad']
            dc=row['dc']
            ma_donnees_hospi_cl_age=donnees_hospi_clage(numero_region,classe_age,jour,hosp,rea,rad,dc)
            donnees_hospi_clage_dao.create(ma_donnees_hospi_cl_age)
    print("Fin6")

    with open('covid_api/csv/donnees_tests_fr.csv', 'r') as f:
        # we don't need the `csv` module.
        reader=csv.DictReader(f, delimiter=';')  # lit ligne à ligne le csv
        for row in reader :
            code_fr = row['fra']
            jour=row['jour']
            positif_femme=row["P_f"]
            positif_homme=row['P_h']
            positif=row['P']
            total_femme = row['T_f']
            total_homme = row['T_h']
            total=row['T']
            classe_age=row['cl_age90']
            ma_donnees_test_fr=donnees_tests_fr(code_fr, jour, positif_femme, positif_homme, positif, total_femme, total_homme, total, classe_age)
            donnees_tests_fr_dao.create(ma_donnees_test_fr)
    print("Fin7")
    print("FIN")