#!/usr/bin/env python3

import os
import traceback
import csv
# pour l'instant le import csv est en gris dans mon code donc il est pas utilisé alors qu'il devrait d'être
import random
import json
import hashlib

from flask import Flask, jsonify, request, Blueprint
from flask_cors import CORS
# en gris donc pas utilisé : je crois que c'est un décorateur
from flask_restplus import Api, Resource
from flask_restplus import abort
from flask_caching import Cache
from loguru import logger
from requests import codes as http_codes
# la je pense y'a un truc à changer car rien sur internet à propos d'une fonction "codes" du package requests
# ça j'ai check a priori rien à toucher c'est pour accéder à notre connexion mais ça à l'air bon
from covid_api.api.commons import configuration
from covid_api.dao.Donnees_hospi_dao import donnees_hospi_dao

print()

CACHE_TTL = 60  # 60 seconds

# Load conf
conf = configuration.load()
script_dir = os.path.dirname(__file__)
# fonction os.path.dirname(path) : Renvoie le nom du répertoire du chemin d'accès (= path en paramètre).
# C'est le premier élément de la paire retourné en passant le chemin (path en paramètre) à la fonction split ().
# i.e : pourquoi on a "__file__" ici : que doit-on mettre ?


def _init_app(p_conf):
    # Load app config into Flask WSGI running instance
    r_app = Flask(__name__)
    r_app.config["API_CONF"] = p_conf
# attribut config de l'objet Flask : on récupère les configuration de base déjà contenu dans Flask
# dans la biblio il explique que la forme doit être
#   app = Flask(__name__)
#   app.config['TESTING'] = True
# du coup je pense pas de problème avec "API_CCONFIG" c'est un choix de non mais le p_conf à la place du "True" me perturbe

    blueprint = Blueprint("api", __name__)
# qu'est-ce qu'un blueprint : un objet qui permet de définir des fonctions d'application sans avoir besoin d'un objet d'application à l'avance.
    # Il utilise les mêmes décorateurs que Flask, mais reporte la nécessité d'une demande en les enregistrant pour une inscription ultérieure.
    # Parameters : name  The name of the blueprint. Will be prepended to each endpoint name.
    # import_name  The name of the blueprint package, usually__name__. This helps locate the root_path for the blueprint.
# donc je pense que les paramètres sont bons
# Le concept de base des blueprints est qu'ils enregistrent les opérations à exécuter lorsqu'ils sont enregistrés sur une application.
# Flask associe les fonctions de vue (view fonctions) à des blueprints lors de la distribution des demandes (requests)
# et de la génération d'URL d'un point de terminaison à un autre.

    r_swagger_api = Api(
        blueprint,
        doc="/" + p_conf["url_prefix"] + "/doc/",
        title="API",
        description="Covid data API",
    )
# fonction api du package flask_restplus -> paramètre
    # app : le bluprint ici "blueprint"
    # version(str) : the API version (used in Swagger documentation) : ici on a rien mis je crois, il propose genre "u'1.0'" dans l'exemple
    # title(str) : titre de l'API
    # description(str) : la deescription de l'API
    # terms_url(str) : l'url de la page des termes de l'API
    # doc(str) : le chemin de la documentation. Si définie sur une valeur fausse (false value), la documentation est désactivée (valeur par défault "/")
    # pleins d'autres paramètres mais non utilisés ici et/ou non importants
    # du coup ce qui peut bloquer c'est le "url_prefix" de l'objet p_conf récupérer par r_app_config dans la fonction _init_app au dessus
    r_app.register_blueprint(blueprint)
    # reprend aussi la fonction _init_app avc l'objet créé r_app qui doit contenir "register_blueprint"
    # qui doit être une fonction à laquelle on associe blueprint => pas très clair
    r_ns = r_swagger_api.namespace(
        name=p_conf["url_prefix"], description="Api documentation"
    )
    #reprend le r_swagger_api ci dessus et appelle l'attribut namespace associé à ce qu'on a entrer
    return r_app, r_swagger_api, r_ns


app, swagger_api, ns = _init_app(conf)
# ça je comprend pas pourquoi c là

# Init cache
cache = Cache(app, config={"CACHE_TYPE": "simple"})
# ici la fonction Cache du package flask_caching qui prend en paramètre "app" et config={"CACHE_TYPE": "simple"}
# en gros ça y'a rien à toucher

# Access log query interceptor
@app.before_request
def access_log():
    logger.info("{0} {1}".format(request.method, request.path))
# comme écris je pense que c'est un truc obligatoire de connexion avant de faire une requête
# provient de "from loguru import logger" -> le "info" est lié a l'attribut "level" de la fonction logger
# cet attribut controle le seuil minimum à partir duquel les log messages sont autorisée à être émis
# -> info est un des niveaux standard possible, il correspond à une valeur de gravité (severty value) de 20
# paramètre de logger.info(message.format(args, kwargs) : ici le message est "{0} {1}", args = request.methode et kwargs= request.path
# kwargs est un paramètre supplémentaire qui n'est valide que pour configurer une coroutine ou un récepteur de fichiers

@ns.route("/", strict_slashes=False)
class Base(Resource):
    @staticmethod
    def get():
        """
            Base route
        """
        response = {"status_code": http_codes.OK, "message": "Api Covid"}

        return make_reponse(response, http_codes.OK)
# Ici on utilise un truc commun : @ns.route est un décorateur utilisé pour spécifier quels URL sera associées à une ressource données
# On spécifie comme paramètre le chemin
# souvent dans le get() il y a plutôt get(self)
# la fonction get usuelle à juste un return grenre get_all_categories(), ici il return le résultat d'une fonction make_response de Flask
# cette fonction est appelée au lieu d'utiliser un retour direct pour obtenir un objet de réponse qu'on peut utiliser pour attacher des en-tête
# c'est quand c'est nécessaire de définir des en-têtre supplémentaire dans une view car les view ne sont pas ensé renvoyer d'objet de réponse
# donc onn lui fait renvouer une valeur qui est convertie en objet de réponse par Flask
# a priori ruen à changer donc, je comprend bof l'utilité du second argument de make_response vu qu'il apparait dans response mais à voir
# peut-être justement que ça fait une vérification



@ns.route("/heartbeat")
class Heart(Resource):
    @staticmethod
    def get():
        """
            Heartbeat
            Are you alive ?
        """
        response = {"status_code": http_codes.OK, "message": "Heartbeat"}

        return make_reponse(response, http_codes.OK)
# pareil d'au dessus avec un message différend

@ns.route("/supervision")
class Supervision(Resource):
    @staticmethod
    def get():
        """
            Api configuration
        """
        response = None
        try:
            response = app.config["API_CONF"]
        except Exception:
            abort(http_codes.SERVER_ERROR, "Can't get the configuration")

        return _success(response)
# on créé d'abord un objet reponse nul qu'on rempli ensuite dans le try / except -> en gros c un agent de vérification pour l'api je pense
# comme dans le titre c une supervision pour permettre une herreur s l'http_codes plante (http_code.OK VS http_code.SERVER_ERROR)

# Doc for covid routes = documentation pour l'itinéraire covid
data_tests_parser = swagger_api.parser()
data_hospitalieres_parser = swagger_api.parser()
# swagger_api a été rentré dans _init_app(conf) tout à l'heure, on en appelle parser et je comprend bof sur le net


@swagger_api.expect(data_hospitalieres_parser)
# permet de spécifier les champs de saisi attendus
@ns.route("/donnees_hospitalieres", endpoint="/donnees_hospitalieres")
# c'est peut-être ici qu'on doit mettre le chemin pour les données hospi ?
class Donnees_hospitalieres(Resource):
    @staticmethod
    #@cache.cached(timeout=CACHE_TTL, key_prefix="/donnees_hospitalieres")
    def get():
        logger.debug("No cache used")
        # comme pour info tout à l'heure, debug est un level associé cette fois à une severity value de 10
        # date par defaut
        date = "01/09/2020"
        if 'date' in request.args:
            date = request.args['date']

        response = donnees_hospi_dao.find_by_jour(date)

        return make_reponse(response, http_codes.OK)

#on dirait pas juste une classe données_hospi mais un get selon la date...
# peut-être que ça utilise la date parce que c'est une clé primaire des données

    @staticmethod
    def post():
        raw = request.json["raw"]
        response = "data created"

        return make_reponse(response, http_codes.OK)
# je crois que le request.json fait appelé la fonction json(kwargs) qui retourne le contenu de la réponse encodé en json si ça existe
# le "raw" semble se référé aux "raw Flask reponse object" mais c pas très clair

@swagger_api.expect(data_tests_parser)
@ns.route("/donnees_tests", endpoint="/donnees_tests")
class Donnees_tests(Resource):
    @staticmethod
    #@cache.cached(timeout=CACHE_TTL, key_prefix="/donnees_hospitalieres")
    def get():
        logger.debug("No cache used")
        # date par defaut
        date = "01/09/2020"
        if 'date' in request.args:
            date = request.args['date']

        response = get_donnees_tests(date)

        return make_reponse(response, http_codes.OK)
# même chose qu'au dessus

@app.after_request
# décorateur qui nous permet de créer une fonction qui sera run après chaque requête :
# les fonctions ci=dessous doivent prendre et retourner des instance de type Flask response class
def set_response_headers(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '0'
    return response
#je sais pas à quoi ça sert mais je pense on y touche pas
def _success(response):
    return make_reponse(response, http_codes.OK)
# ça indique logiquement le succès

def _failure(exception, http_code=http_codes.SERVER_ERROR):
    try:
        exn = traceback.format_exc(exception)
        logger.info("EXCEPTION: {}".format(exn))
    except Exception as e:
        logger.info("EXCEPTION: {}".format(exception))
        logger.info(e)

    try:
        data, code = exception.to_tuple()
        return make_reponse(data, code)
    except:
        try:
            data = exception.to_dict()
            return make_reponse(data, exception.http)
        except Exception:
            return make_reponse(None, http_code)
# et ça l'échec en lien avec ce qu'on avait définie plus haut au niveau de la classe Supervision et du cas Exception

def make_reponse(p_object=None, status_code=http_codes.OK):
    if p_object is None and status_code == http_codes.NOT_FOUND:
        p_object = {
            "status": {
                "status_content": [
                    {"code": "404 - Not Found", "message": "Resource not found"}
                ]
            }
        }

    json_response = jsonify(p_object)
    json_response.status_code = status_code
    json_response.content_type = "application/json;charset=utf-8"
    json_response.headers["Cache-Control"] = "max-age=3600"
    return json_response
# dans le make_response donc j'ai déjà parlé plus haut il y a effectivement comme paramètre make_response(res, code, headers)
# on suppose ici qu'on a gardé res=p_object et code=status_code
# y'a encore un truc de vérification avec le if et après on utilise jsonify qui est apparemment une fonction pour "faciliter la vie"
# elle transforme la sortie JSON en un objet Response avec le type application/json/mimetype et en plus, il convertit plusieurs arguments
# d'un tableau ou de mots clés en dictionnaire (dict)
# ex : jsonify(1,2,3) et jsonify([1,2,3]) sérialisent tous les deux en [1,2,3]
# il prend en argument jsonify(args, kwargs)
# le reste je pense pas de pb

def get_donnees_hospitalieres(p_date: str) -> dict:
# alors ça le " -> dict" c du javanais, j'avais jamais vu avant, peut-être que ça indique que les données vont être transformée en dictionnaire
# get_donnees_hospitalieres(date) = response dans la fonction get() de la classe Donnees_hospitalires(Ressource) d'en haut
# je suppose que p_date est un type de p_objet définie plus haut
    try:

        response = {
            "donnees_hospitalieres": [
                {
                    "date": "01/10/2020",
                    "departement": "35",
                    "sexe": 1,
                    "hospitalises": 17,
                    "reanimation": 2,
                    "retour_a_domicile": 6,
                    "deces": 1
                },
                {
                    "date": "01/10/2020",
                    "departement": "35",
                    "sexe": 2,
                    "hospitalises": 17,
                    "reanimation": 2,
                    "retour_a_domicile": 6,
                    "deces": 1
                },
                {
                    "date": "01/10/2020",
                    "departement": "22",
                    "sexe": 1,
                    "hospitalises": 15,
                    "reanimation": 2,
                    "retour_a_domicile": 4,
                    "deces": 1
                },
                {
                    "date": "01/10/2020",
                    "departement": "29",
                    "sexe": 1,
                    "hospitalises": 22,
                    "reanimation": 2,
                    "retour_a_domicile": 6,
                    "deces": 1
                }
            ],
        }
        logger.info(response)

        #response = json.dumps(result[1:4]) # Tous les elements de 1 a 3

        return response
    except Exception:
        abort(http_codes.SERVER_ERROR, "Can't get covid data")
# je pense il test des truc mais pas sûre
# pour abort : lié à la fonction de Flask abort(status, args, kwargs) qui déclenche une HTTPException pour le code d'état ou l'application donné
# si un code d'état est donné, il sera recherché dans la liste des exceptions et délencheracette exception

def get_donnees_tests(p_date: str) -> dict:
    try:

        response = {
            "donnees_tests": [
                {
                    "date": "01/10/2020",
                    "departement": "35",
                    "nombre_tests": 16,
                    "nombre_tests_positifs": 0,
                    "classe_age": "9"
                },
                {
                    "date": "01/10/2020",
                    "departement": "35",
                    "nombre_tests": 23,
                    "nombre_tests_positifs": 2,
                    "classe_age": "19"
                },
                {
                    "date": "01/10/2020",
                    "departement": "35",
                    "nombre_tests": 37,
                    "nombre_tests_positifs": 2,
                    "classe_age": "29"
                }
            ],
        }
        logger.info(response)

        #response = json.dumps(result[1:4]) # Tous les elements de 1 a 3

        return response
    except Exception:
        abort(http_codes.SERVER_ERROR, "Can't get covid data")
# même chose que plus haut

if __name__ == "__main__":
    #cf_port = os.getenv("PORT")
    cf_port = conf["port"]
    if cf_port is None:
        app.run(host="localhost", port=5001, debug=True)
    else:
        app.run(host="localhost", port=int(cf_port), debug=True)
# conf définie au tout début comme configuration.load() qui provient d'un autre fichier de covid_api,
# notre "port" c 9090 selon le fichier api-conf du dossier
# donc comme 9090 différent de 5001 proposé si cf_port is None je suppose que c'est un truc de débug si le port est faux