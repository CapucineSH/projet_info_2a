from PyInquirer import prompt
from view.abstract_view import Abstract_view
from datetime import date, timedelta  # pour gérer les dates
from prompt_toolkit.validation import Validator, ValidationError
import regex

class param_jour_hospi_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'input',
                'name': 'menu',
                'message':'Entrez une date, format AAAA-MM-JJ',
                'validate' : DateValidate
            }
        ]

    def display_info(self):
        print('Période possédant des données : {}'.format(Abstract_view.session.periode))
        print('Date actuelle : {}'.format(Abstract_view.session.date))

    def make_choice(self):
        resp = prompt(self.questions)
        print(resp)
        from view.Parametrages_hospi_view import parametrages_hospi_view
        return parametrages_hospi_view()

class DateValidate(Validator):
    """
    Classe qui va nous permettre de valider la date. Validator est une classe abstraite qui
    nous demande de définir la méthode validate
    """
    @staticmethod
    def validate_format(date):
        """
        Valide uniquement le format
        """
        ok = regex.match('\d{4}-\d{2}-\d{2}', date.text)
        return ok
    @staticmethod
    def validate_periode(document):
        """
        Valide uniquement si la date est dans la période.
        """
        YYYY = int(document.text[0:4])
        MM = int(document.text[5:7])
        DD = int(document.text[8:10])
        doc_date = date(YYYY,MM,DD)
        ok = doc_date > date.today() - timedelta(days = 4) or doc_date < date(year = 2020, month = 3, day = 18)
        return ok

    def validate(self, document):
        """
        Méthode à définir, qui permet de valider la date
        Document est un objet gérer par PyInquirer
        Voilà ce que contient la doctring de la méthode dans la classe mère :
            Validate the input.
            If invalid, this should raise a :class:`.ValidationError`.
            :param document: :class:`~prompt_toolkit.document.Document` instance.
        """
        if not DateValidate.validate_format(document):
            raise ValidationError(
                message='Le format n\'est pas valide. Réessayez',
                cursor_position=len(document.text)
            )
        elif DateValidate.validate_periode(document):
            raise ValidationError(
                message='Les données ne sont pas accessibles à la date indiquée. Réessayez',
                cursor_position=len(document.text)
            )
        return True
