from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view
from datetime import date, timedelta

class param_date_tests_view (Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Voulez-vous regarder les informations sur un jour ou sur une période ?',
                'choices': [
                    'Tout',
                    'Un jour',
                    'Une période',
                    Separator(),
                    'Retourner au menu de paramétrage'

                ]
            }
        ]

    def display_info(self):
        print('Date actuelle : {}'.format(Abstract_view.session.periode))

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Un jour':
            from view.Param_jour_tests_view import param_jour_tests_view
            return param_jour_tests_view() # lance le nouveau menu
        elif resp['menu'] == 'Une période':
            from view.Param_periode_tests_view import param_periode_tests_view
            return param_periode_tests_view() # lance le nouveau menu
        elif resp['menu'] == 'Tout':
            Abstract_view.session.periode = [date.isoformat(date.today() - timedelta(4)),
                                            date.isoformat(date(year=2020, month=5, day=13))]
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()
        else:
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()

