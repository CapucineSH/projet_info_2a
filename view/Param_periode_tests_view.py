from PyInquirer import prompt
from view.abstract_view import Abstract_view
from datetime import date  # pour gérer les dates
from view.Param_jour_tests_view import DateValidate

class param_periode_tests_view(Abstract_view):
    def __init__(self):
        self.questions1 = [
            {
                'type': 'input',
                'name': 'menu1',
                'message': 'Entrez une première date, format AAAA-MM-JJ',
                'validate': DateValidate
            }
        ]
        self.questions2 = [
            {
                'type': 'input',
                'name': 'menu2',
                'message': 'Entrez une deuxième date, format AAAA-MM-JJ',
                'validate': DateValidate
            }
        ]

    def display_info(self):
        print('Date actuelle : {}'.format(Abstract_view.session.periode))

    def make_choice(self):
        resp1 = prompt(self.questions1)
        resp2 = prompt(self.questions2)
        resp1 = resp1['menu1']
        resp2 = resp2['menu2']
        YYYY = int(resp1[0:4])
        MM = int(resp1[5:7])
        DD = int(resp1[8:10])
        date_1 = date(YYYY, MM, DD)
        YYYY2 = int(resp2[0:4])
        MM2 = int(resp2[5:7])
        DD2 = int(resp2[8:10])
        date_2 = date(YYYY2, MM2, DD2)
        if date_1 > date_2:
            Abstract_view.session.periode =[resp2, resp1]
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()
        elif date_2 > date_1:
            Abstract_view.session.periode =[resp1, resp2]
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()
        else:
            Abstract_view.session.date = resp1
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()
