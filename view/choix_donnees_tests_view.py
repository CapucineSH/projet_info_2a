from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view

class Choix_donnees_tests_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Quel(s) indicateur(s) souhaitez-vous calculer ?',
                'choices': [
                    'Nombre de tests réalisés',
                    'Nombre de tests positifs',
                    'Taux d\'incidence / Activité épidémique',
                    'Taux de positivité des tests virologiques',
                    Separator(),
                    'Tous',
                    Separator(),
                    'Valider la sélection',
                    Separator(),
                    'Retourner au menu des données de test'
                ]
            }
        ]

    def display_info(self):
        if Abstract_view.session.indicateurs == []:
            print('Aucun indicateur n\'a été sélectionné')
        else:
            print('Indicateurs sélectionnés :')
            for i in Abstract_view.session.indicateurs:
                print(i)  # le nom de l'indicateur

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Retourner au menu des données de test':
            Abstract_view.session.indicateurs = []
            # efface la sélection
            from view.donnees_tests_view import Donnees_tests_view
            return Donnees_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Tous':
            Abstract_view.session.indicateurs = ['t', 'p', 'tauxincid', 'tauxpos']
            # tous les paramètres sont sélectionnés. à remplacer si on opte pour des classes
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Nombre de tests réalisés':
            if 't' not in Abstract_view.session.indicateurs:
                Abstract_view.session.indicateurs.append('t')
            return Choix_donnees_tests_view() # relance le menu
        if resp['menu'] == 'Nombre de tests positifs':
            if 'p' not in Abstract_view.session.indicateurs:
                Abstract_view.session.indicateurs.append('p')
            return Choix_donnees_tests_view() # relance le menu
        if resp['menu'] == 'Taux d\'incidence / Activité épidémique':
            if 'tauxincid' not in Abstract_view.session.indicateurs:
                Abstract_view.session.indicateurs.append('tauxincid')
            return Choix_donnees_tests_view() # relance le menu
        if resp['menu'] == 'Taux de positivité des tests virologiques':
            if 'tauxpos' not in Abstract_view.session.indicateurs:
                Abstract_view.session.indicateurs.append('tauxpos')
            return Choix_donnees_tests_view() # relance le menu
        if resp['menu'] == 'Valider la sélection':
            if Abstract_view.session.indicateurs == []:
                print('Veuillez sélectionner au moins un indicateur')
                return Choix_donnees_tests_view()
            else:
                from view.parametrage_tests_view import Parametrage_tests_view
                return Parametrage_tests_view()  # lance le nouveau menu