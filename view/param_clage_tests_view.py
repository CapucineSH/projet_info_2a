from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view

class Param_clage_tests_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Sur quelle classe d\'âge souhaitez-vous afficher les résultats ?',
                'choices': [
                    'Toutes',
                    '0 à 9 ans',
                    '10 à 19 ans',
                    '20 à 29 ans',
                    '30 à 39 ans',
                    '40 à 49 ans',
                    '50 à 59 ans',
                    '60 à 69 ans',
                    '70 à 79 ans',
                    '80 à 89 ans',
                    '90 ans et plus',
                    Separator(),
                    'Retourner au menu des paramètres'
                ]
            }
        ]

    def display_info(self):
        print('Classe d\'âge actuelle : {}'.format(Abstract_view.session.clage))

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Toutes':
            Abstract_view.session.clage = 0
        if resp['menu'] == '0 à 9 ans':
            Abstract_view.session.clage = 9
        if resp['menu'] == '10 à 19 ans':
            Abstract_view.session.clage = 19
        if resp['menu'] == '20 à 29 ans':
            Abstract_view.session.clage = 29
        if resp['menu'] == '30 à 39 ans':
            Abstract_view.session.clage = 39
        if resp['menu'] == '40 à 49 ans':
            Abstract_view.session.clage = 49
        if resp['menu'] == '50 à 59 ans':
            Abstract_view.session.clage = 59
        if resp['menu'] == '60 à 69 ans':
            Abstract_view.session.clage = 69
        if resp['menu'] == '70 à 79 ans':
            Abstract_view.session.clage = 79
        if resp['menu'] == '80 à 89 ans':
            Abstract_view.session.clage = 89
        if resp['menu'] == '90 ans et plus':
            Abstract_view.session.clage = 90
        # on retourne au menu précédent dans tous les cas
        from view.parametrage_tests_view import Parametrage_tests_view
        return Parametrage_tests_view()  # lance le nouveau menu