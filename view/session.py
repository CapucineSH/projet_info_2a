from datetime import date, timedelta  # pour avoir la date par défaut
from Services.Indicatif_region_service import indicatif_region_service
from Services.Departements_region_service import departements_region_service

class Session:
    def __init__(self):
        """
        Définition des variables stockées en session et qui pourront être
        utiles à plusieurs reprises tant que la session est active
        """
        self.indicateurs = []  # liste des indicateurs sélectionnés
        self.clage = '0'  # par défaut : pas de classe d'âge
        self.sexe ='0'  # par défaut : pas de sexe
        self.date = date.today() - timedelta(4)
        self.periode =[date.isoformat(date.today() - timedelta(4)) ,date.isoformat(date(year = 2020, month = 5, day = 13))] # par défaut : depuis le début
        zonegeo_affiche = \
            indicatif_region_service.get_all_donnee_from_db()
        zonegeo2_affiche = \
            departements_region_service.get_all_donnee_from_db()
        self.zonegeoreg = [donnee.num_reg for donnee in zonegeo_affiche]
        self.zonegeodep = [donnee.num_dep for donnee in zonegeo2_affiche]
        self.isreg = True

