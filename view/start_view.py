# -*- coding: ISO-8859-1 -*-

from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view

class StartView(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Quelles donn�es souhaitez-vous observer ?',
                'choices': [
                    'Donn�es hospitali�res',
                    'Donn�es des tests',
                    Separator(),
                    'Quitter l\'application'
                ]
            }
        ]

    def display_info(self):
        with open('assets/banner.txt', 'r', encoding="utf-8") as asset:
            print(asset.read())

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Donn�es hospitali�res':
            from view.Donnees_hospi_view import Donnees_hospi_view
            return Donnees_hospi_view()  # lance le nouveau menu
        if resp['menu'] == 'Donn�es des tests':
            from view.donnees_tests_view import Donnees_tests_view
            return Donnees_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Quitter l\'application':
            print('Merci d\'avoir utilis� l\'application')
