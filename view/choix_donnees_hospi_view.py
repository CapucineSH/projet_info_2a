from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view


class Choix_donnees_hospi_view (Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Quel indicateur souhaitez-vous calculer ?',
                'choices': [
                    'Nombre cumulé de personnes de retour à domicile',
                    'Nombre de personne actuellement en réanimation ou soins intensifs',
                    'Nombre de personnes actuellement hospitalisées',
                    'Nombre cumulé de décès à l\'hopital',
                    Separator(),
                    'Tous',
                    Separator(),
                    'Valider la sélection',
                    Separator(),
                    'Retourner au menu des données hospitalières'


                ]
            }
        ]

    def display_info(self):
        if Abstract_view.session.indicateurs == []:
            print('Aucun indicateur n\'a été sélectionné')
        else:
            print('Indicateurs sélectionnés :')
            for i in Abstract_view.session.indicateurs:
                print(i)  # le nom de l'indicateur


    def make_choice(self):
        resp = prompt(self.questions)

        if resp['menu'] == 'Retourner au menu des données hospitalières':
            Abstract_view.session.indicateurs = []
            # efface la sélection
            from view.Donnees_hospi_view import Donnees_hospi_view
            return Donnees_hospi_view()  # lance le nouveau menu
        if resp['menu'] == 'Tous':
            Abstract_view.session.indicateurs = ['rad', 'rea', 'hosp', 'dc']
            # tous les paramètres sont sélectionnés. à remplacer si on opte pour des classes
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()  # lance le nouveau menu
        if resp['menu'] == 'Nombre cumulé de personnes de retour à domicile':
            if 'rad' not in Abstract_view.session.indicateurs:
                Abstract_view.session.indicateurs.append('rad')

            return Choix_donnees_hospi_view() # relance le menu
        if resp['menu'] == 'Nombre de personne actuellement en réanimation ou soins intensifs':
            if 'rea' not in Abstract_view.session.indicateurs:
                Abstract_view.session.indicateurs.append('rea')

            return Choix_donnees_hospi_view() # relance le menu
        if resp['menu'] == 'Nombre de personnes actuellement hospitalisées':
            if 'hosp' not in Abstract_view.session.indicateurs:
                Abstract_view.session.indicateurs.append('hosp')

            return Choix_donnees_hospi_view() # relance le menu
        if resp['menu'] == 'Nombre cumulé de décès à l\'hopital':
            if 'dc' not in Abstract_view.session.indicateurs:
                Abstract_view.session.indicateurs.append('dc')

            return Choix_donnees_hospi_view() # relance le menu
        if resp['menu'] == 'Valider la sélection':
            if Abstract_view.session.indicateurs == []:
                print('Veuillez sélectionner au moins un indicateur')
                return Choix_donnees_hospi_view()
            else:
                from view.Parametrages_hospi_view import parametrages_hospi_view
                return parametrages_hospi_view()  # lance le nouveau menu