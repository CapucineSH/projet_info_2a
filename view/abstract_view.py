from abc import ABC, abstractmethod
from view.session import Session

class Abstract_view(ABC):
    session = Session()

    @abstractmethod
    def display_info(self): # sera redéfini pour chaque menu
        pass

    @abstractmethod
    def make_choice(self): # sera redéfini pour chaque menu
        pass