from PyInquirer import Separator, prompt
from datetime import date, timedelta  # pour avoir la date par défaut

from view.abstract_view import Abstract_view
from Services.Donnees_hospi_service import donnees_hospi_service
from Services.Donnees_hospi_clage_service import donnees_hospi_clage_service


class tout_hospi_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Que voulez vous faire ? :',
                'choices': [
                    'Changer les paramètres',
                    Separator(),
                    'Menu Principal'
                ]
            }
        ]

    def display_info(self):
        print('Paramètres actuels :')
        print(Abstract_view.session.indicateurs)
        print('Classe d\'âge : {}'.format(Abstract_view.session.clage))
        print('Sexe : {}'.format(Abstract_view.session.sexe))
        print('Période : {}'.format(Abstract_view.session.periode))
        print('Zone géographique (région) : {}'.format(Abstract_view.session.zonegeoreg))
        print('Zone géographique (département) : {}'.format(Abstract_view.session.zonegeodep))

        if Abstract_view.session.clage != '0' :
            if Abstract_view.session.date != date.today() - timedelta(4):
                toute_mes_donnees = donnees_hospi_clage_service.get_all_donnee_from_db()
                donnee_a_afficher = []
                for donnee in toute_mes_donnees:
                    if toute_mes_donnees[0] == Abstract_view.session.zonegeoreg and \
                            toute_mes_donnees[1] == Abstract_view.session.clage and \
                            toute_mes_donnees[2] == Abstract_view.session.date and \
                            toute_mes_donnees[3] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[4] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[5] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[6] in Abstract_view.session.indicateurs :
                        donnee_a_afficher.append(donnee)
                    print(toute_mes_donnees)
            elif Abstract_view.session.periode != [date.isoformat(date.today() - timedelta(4)) ,date.isoformat(date(year = 2020, month = 5, day = 13))]:
                toute_mes_donnees = donnees_hospi_clage_service.get_all_donnee_from_db()
                donnee_a_afficher = []
                for donnee in toute_mes_donnees:
                    if toute_mes_donnees[0] == Abstract_view.session.zonegeoreg and \
                            toute_mes_donnees[1] == Abstract_view.session.clage and \
                            toute_mes_donnees[2] in Abstract_view.session.periode and \
                            toute_mes_donnees[3] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[4] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[5] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[6] in Abstract_view.session.indicateurs:
                        donnee_a_afficher.append(donnee)
                    print(toute_mes_donnees)
            elif Abstract_view.session.date == date.today() - timedelta(4) and \
                Abstract_view.session.periode == [date.isoformat(date.today() - timedelta(4)) ,date.isoformat(date(year = 2020, month = 5, day = 13))]:
                toute_mes_donnees = donnees_hospi_clage_service.get_all_donnee_from_db()
                donnee_a_afficher = []
                for donnee in toute_mes_donnees:
                    if toute_mes_donnees[0] == Abstract_view.session.zonegeoreg and \
                            toute_mes_donnees[1] in Abstract_view.session.clage and \
                            toute_mes_donnees[3] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[4] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[5] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[6] in Abstract_view.session.indicateurs :
                        donnee_a_afficher.append(donnee)
                    print(toute_mes_donnees)
        else :
            if Abstract_view.session.date != date.today() - timedelta(4):
                toute_mes_donnees = donnees_hospi_service.get_all_donnee_from_db()
                donnee_a_afficher = []
                for donnee in toute_mes_donnees:
                    if toute_mes_donnees[0] == Abstract_view.session.zonegeodep and \
                            toute_mes_donnees[1] == Abstract_view.session.sexe and \
                            toute_mes_donnees[2] == Abstract_view.session.date and \
                            toute_mes_donnees[3] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[4] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[5] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[6] in Abstract_view.session.indicateurs:
                        donnee_a_afficher.append(donnee)
                    print(toute_mes_donnees)
            elif Abstract_view.session.periode != [date.isoformat(date.today() - timedelta(4)),
                                                   date.isoformat(date(year=2020, month=5, day=13))]:
                toute_mes_donnees = donnees_hospi_service.get_all_donnee_from_db()
                donnee_a_afficher = []
                for donnee in toute_mes_donnees:
                    if toute_mes_donnees[0] == Abstract_view.session.zonegeodep and \
                            toute_mes_donnees[1] == Abstract_view.session.sexe and \
                            toute_mes_donnees[2] in Abstract_view.session.periode and \
                            toute_mes_donnees[3] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[4] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[5] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[6] in Abstract_view.session.indicateurs:
                        donnee_a_afficher.append(donnee)
                    print(toute_mes_donnees)
            elif Abstract_view.session.date == date.today() - timedelta(4) and \
                    Abstract_view.session.periode == [date.isoformat(date.today() - timedelta(4)),
                                                      date.isoformat(date(year=2020, month=5, day=13))]:
                toute_mes_donnees = donnees_hospi_service.get_all_donnee_from_db()
                donnee_a_afficher = []
                for donnee in toute_mes_donnees:
                    if toute_mes_donnees[0] == Abstract_view.session.zonegeodep and \
                            toute_mes_donnees[1] in Abstract_view.session.sexe and \
                            toute_mes_donnees[3] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[4] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[5] in Abstract_view.session.indicateurs and \
                            toute_mes_donnees[6] in Abstract_view.session.indicateurs:
                        donnee_a_afficher.append(donnee)
                    print(toute_mes_donnees)



    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse["menu"] == 'Changer les paramètres':
            from view.donnees_tests_view import Donnees_tests_view
            return Donnees_tests_view()
        else:
            from view.start_view import StartView
            return StartView()



