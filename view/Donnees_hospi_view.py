from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view

class Donnees_hospi_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Que souhaitez-vous faire ?',
                'choices': [
                    'Choisir les données et l\'affinage',
                    Separator(),
                    'Retourner au menu principal'
                ]
            }
        ]

    def display_info(self):
        print('Les données hospitalières peuvent être observées ici')
        # devra sûrement être remplacé par un affichage du paramétrage

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Choisir les données et l\'affinage':
            from view.choix_donnees_hospi_view import Choix_donnees_hospi_view
            return Choix_donnees_hospi_view() # lance le nouveau menu
        else:
            from view.start_view import StartView
            return StartView()  # lance le nouveau menu