from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view
from csv import reader

class Param_zone_tests_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Quelle zone géographique souhaitez-vous observer ?',
                'choices': [
                    'Un département',
                    'Une région ou DROM',
                    'France entière',
                    'France métropolitaine',
                    Separator(),
                    'Retourner au menu des paramètres'
                ]
            }
        ]

    def display_info(self):
        print('Zone géographique actuelle (région) : {}'.format(Abstract_view.session.zonegeoreg))
        print('Zone géographique actuelle (département) : {}'.format(Abstract_view.session.zonegeodep))

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Retourner au menu des paramètres':
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'France entière':
            # france entière (toutes régions)
            file = open("covid_api/csv/indicatif_region.csv")
            file2 = open("covid_api/csv/table_pop_dep_reg.csv")
            liste = list(reader(file, delimiter=","))
            liste2 = list(reader(file2, delimiter=","))
            Abstract_view.session.zonegeoreg = [int(elt[0]) for elt in liste[1:]]
            Abstract_view.session.zonegeodep = [elt[2] for elt in liste2[1:]]
            Abstract_view.session.isreg = True
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()
        if resp['menu'] == 'France métropolitaine':
            # france métropolitaine (toutes régions sauf drom)
            file = open("covid_api/csv/indicatif_region.csv")
            file2 = open("covid_api/csv/table_pop_dep_reg.csv")
            liste = list(reader(file, delimiter=","))
            liste2 = list(reader(file2, delimiter=","))
            Abstract_view.session.zonegeoreg = [int(elt[0]) for elt in liste[6:]]
            Abstract_view.session.zonegeodep = [elt[2] for elt in liste2[2:-5]]
            Abstract_view.session.isreg = True
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Tous':
            # retour au menu précédent
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Une région ou DROM':
            # sous-menu région
            return Param_zone_reg_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Un département':
            # département et sexe incompatibles : vérification
            if Abstract_view.session.sexe == '0':
                return Param_zone_depreg_tests_view()
            elif Abstract_view.session.sexe != '0':
                return Param_zone_depconf_tests_view()  # lance le nouveau menu

class Param_zone_depconf_tests_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'confirm',
                'name': 'menu',
                'message': 'Souhaitez-vous continuer ? Votre paramètre sexe sera rétabli à 0 : Tous.'
            }
        ]

    def display_info(self):
        print('Les données par département ne sont pas disponibles par sexe. Or le paramètre sexe est établi à {}.'.format(Abstract_view.session.sexe))

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == False:
            return Param_zone_tests_view()  # lance le nouveau menu
        if resp['menu'] == True:
            Abstract_view.session.sexe = '0'
            return Param_zone_depreg_tests_view()

class Param_zone_depreg_tests_view(Abstract_view):
    def __init__(self):
        file = open("covid_api/csv/indicatif_region.csv")
        liste = list(reader(file, delimiter=","))
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Dans quelle région se situe le département ?',
                'choices': [elt[4] for elt in liste[6:]]+[
                    Separator(),
                    'Retour au choix de la zone'
                ]
            }
        ]

    def display_info(self):
        print('')

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Retour au choix de la zone':
            return Param_zone_tests_view()  # lance le nouveau menu
        else:
            return Param_zone_depdep_tests_view(resp['menu'])

class Param_zone_depdep_tests_view(Abstract_view):
    def __init__(self, reg):
        file = open("covid_api/csv/table_pop_dep_reg.csv")
        liste = list(reader(file, delimiter=","))
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Quel département souhaitez-vous observer ?',
                'choices': [elt[0] for elt in liste[1:] if elt[3] == reg]+[
                    Separator(),
                    'Retour au choix de la région'
                ]
            }
        ]
        self.reg = reg

    def display_info(self):
        print('')

    def make_choice(self):
        resp = prompt(self.questions)
        file = open("covid_api/csv/table_pop_dep_reg.csv")
        liste = list(reader(file, delimiter=","))
        if resp['menu'] == 'Retour au choix de la région':
            return Param_zone_depreg_tests_view()  # lance le nouveau menu
        else:
            Abstract_view.session.isreg = False
            Abstract_view.session.zonegeodep = [[elt[2] for elt in liste[1:]][[elt[0] for elt in liste[1:]].index(resp['menu'])]]
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()  # lance le nouveau menu

class Param_zone_reg_tests_view(Abstract_view):
    def __init__(self):
        file = open("covid_api/csv/indicatif_region.csv")
        liste = list(reader(file, delimiter=","))
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Quelle région souhaitez-vous observer ?',
                'choices': [elt[4] for elt in liste[1:]]+[
                    Separator(),
                    'Retour au choix de la zone'
                ]
            }
        ]

    def display_info(self):
        print('')

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] != 'Retour au choix de la zone':
            file = open("covid_api/csv/indicatif_region.csv")
            liste = list(reader(file, delimiter=","))
            Abstract_view.session.isreg = True
            Abstract_view.session.zonegeoreg = [[elt[0] for elt in liste[1:]][[elt[4] for elt in liste[1:]].index(resp['menu'])]]
        from view.parametrage_tests_view import Parametrage_tests_view
        return Parametrage_tests_view()