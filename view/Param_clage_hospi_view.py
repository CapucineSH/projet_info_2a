from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view
from Services.Label_clage_service import label_clage_service
from Services.Departements_region_service import departements_region_service


class Param_clage_hospi_view(Abstract_view):
    def __init__(self):
        self.clage_affiche = \
            label_clage_service.get_all_donnee_from_db();
        self.clage_nom = [donnee.nom_clage for donnee in self.clage_affiche]
        choix_donnees = self.clage_nom
        choix_donnees.append(Separator())
        choix_donnees.append("Retour menu paramétrage")
        self.questions = [
            {
                'type':'list',
                'name':'donnee hospi',
                'message': 'Veuillez choisir une classe d\'âge',
                'choices': choix_donnees
            }
        ]

    def display_info(self):
        print('Classe d\'âge actuelle : {}'.format(Abstract_view.session.clage))


    def make_choice(self):
        resp = prompt(self.questions)
        if resp['donnee hospi'] == "Retour menu paramétrage":
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()  # lance le nouveau menu
        if resp['donnee hospi'] == "Toutes":
            Abstract_view.session.clage = '0'
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()  # lance le nouveau menu
        else :
            # département et classe d'âge incompatibles : vérification
            if Abstract_view.session.isreg:
                Abstract_view.session.clage = \
                    label_clage_service.get_donnee_from_db_by_nom_clage(resp["donnee hospi"]).label
                from view.Parametrages_hospi_view import parametrages_hospi_view
                return parametrages_hospi_view()
            elif not(Abstract_view.session.isreg):
                return Param_clage_conf_hospi_view(resp['donnee hospi'])  # lance le nouveau menu



class Param_clage_conf_hospi_view(Abstract_view):
    def __init__(self, clagemem):
        self.questions = [
            {
                'type': 'confirm',
                'name': 'menu',
                'message': 'Souhaitez-vous continuer ? Votre paramètre zone sera rétabli à la valeur par défaut (tous les départements et toutes les régions).'
            }
        ]
        self.clagemem = clagemem

    def display_info(self):
        print('Les données par classe d\'âge ne sont pas disponibles par département. Or la zone sélectionnée est un département.')

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == False:
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()
        if resp['menu'] == True:
            zonegeo2_affiche = \
                departements_region_service.get_all_donnee_from_db()
            Abstract_view.session.zonegeodep = [donnee.num_dep for donnee in zonegeo2_affiche]
            Abstract_view.session.clage = \
                label_clage_service.get_donnee_from_db_by_nom_clage(self.clagemem).label
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()