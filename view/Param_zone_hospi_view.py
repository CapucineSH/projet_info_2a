from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view
from csv import reader

class Param_zone_hospi_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Quelle zone géographique souhaitez-vous observer ?',
                'choices': [
                    'Un département',
                    'Une région ou DROM',
                    'France entière',
                    'France métropolitaine',
                    Separator(),
                    'Retourner au menu des paramètres'
                ]
            }
        ]


    def display_info(self):
        print('Zone géographique actuelle (région): {}'.format(Abstract_view.session.zonegeoreg))
        print('Zone géographique actuelle (département): {}'.format(Abstract_view.session.zonegeodep))

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Retourner au menu des paramètres':
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()  # lance le nouveau menu
        if resp['menu'] == 'France entière':
            # france entière (toutes régions)
            file = open("covid_api/csv/indicatif_region.csv")
            file2 = open("covid_api/csv/table_pop_dep_reg.csv")
            liste = list(reader(file, delimiter=","))
            liste2 = list(reader(file2, delimiter=","))
            Abstract_view.session.zonegeoreg = [int(elt[0]) for elt in liste[1:]]
            Abstract_view.session.zonegeodep = [elt[2] for elt in liste2[1:]]
            Abstract_view.session.isreg = True
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()  # lance le nouveau menu
        if resp['menu'] == 'France métropolitaine':
            # france métropolitaine (toutes régions sauf drom)
            file = open("covid_api/csv/indicatif_region.csv")
            file2 = open("covid_api/csv/table_pop_dep_reg.csv")
            liste = list(reader(file, delimiter=","))
            liste2 = list(reader(file2, delimiter=","))
            Abstract_view.session.zonegeoreg = [int(elt[0]) for elt in liste[6:]]
            Abstract_view.session.zonegeodep = [elt[2] for elt in liste2[2:-5]]
            Abstract_view.session.isreg = True
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()  # lance le nouveau menu
        if resp['menu'] == 'Une région ou DROM':
            return Param_zone_reg_hospi_view()
        if resp['menu'] == 'Un département':
            # departement et classe d'âge incompatible : verification
            if Abstract_view.session.clage == '0':
                return Param_zone_depreg_hospi_view()
            elif Abstract_view.session.clage != '0':
                return Param_zone_depconf_hospi_view()

class Param_zone_depconf_hospi_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'confirm',
                'name': 'menu',
                'message': 'Souhaitez-vous continuer ? Votre paramètre classe d\'âge sera rétabli à 0 : Tous.'
            }
        ]

    def display_info(self):
        print('Les données par département ne sont pas disponibles par classe d\'âge. Or le paramètre classe d\'âge est établi à : entre {}.'.format(Abstract_view.session.clage))

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == False:
            return Param_zone_hospi_view()  # lance le nouveau menu
        if resp['menu'] == True:
            Abstract_view.session.clage = '0'
            return Param_zone_depreg_hospi_view()

class Param_zone_depreg_hospi_view(Abstract_view):
    def __init__(self):
        file = open("covid_api/csv/indicatif_region.csv")
        liste = list(reader(file, delimiter=","))
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Dans quelle région se situe le département ?',
                'choices': [elt[4] for elt in liste[6:]]+[
                    Separator(),
                    'Retour au choix de la zone'
                ]
            }
        ]

    def display_info(self):
        print('')

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Retour au choix de la zone':
            return Param_zone_hospi_view()  # lance le nouveau menu
        else:
            return Param_zone_depdep_hospi_view(resp['menu'])

class Param_zone_depdep_hospi_view(Abstract_view):
    def __init__(self, reg):
        file = open("covid_api/csv/table_pop_dep_reg.csv")
        liste = list(reader(file, delimiter=","))
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Quel département souhaitez-vous observer ?',
                'choices': [elt[0] for elt in liste[1:] if elt[3] == reg]+[
                    Separator(),
                    'Retour au choix de la région'
                ]
            }
        ]
        self.reg = reg

    def display_info(self):
        print('')

    def make_choice(self):
        resp = prompt(self.questions)
        file = open("covid_api/csv/table_pop_dep_reg.csv")
        liste = list(reader(file, delimiter=","))
        if resp['menu'] == 'Retour au choix de la région':
            return Param_zone_depreg_hospi_view()  # lance le nouveau menu
        else:
            Abstract_view.session.isreg = False
            Abstract_view.session.zonegeodep = [[elt[2] for elt in liste[1:]][[elt[0] for elt in liste[1:]].index(resp['menu'])]]
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()  # lance le nouveau menu

class Param_zone_reg_hospi_view(Abstract_view):
    def __init__(self):
        file = open("covid_api/csv/indicatif_region.csv")
        liste = list(reader(file, delimiter=","))
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Quelle région souhaitez-vous observer ?',
                'choices': [elt[4] for elt in liste[1:]]+[
                    Separator(),
                    'Retour au choix de la zone'
                ]
            }
        ]

    def display_info(self):
        print('')

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] != 'Retour au choix de la zone':
            file = open("covid_api/csv/indicatif_region.csv")
            liste = list(reader(file, delimiter=","))
            Abstract_view.session.isreg = True
            Abstract_view.session.zonegeoreg = [[elt[0] for elt in liste[1:]][[elt[4] for elt in liste[1:]].index(resp['menu'])]]
        from view.Parametrages_hospi_view import parametrages_hospi_view
        return parametrages_hospi_view()  # lance le nouveau menu