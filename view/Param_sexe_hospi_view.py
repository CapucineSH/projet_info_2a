from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view
from Services.Label_sexe_service import label_sexe_service

class Param_sexe_hospi_view(Abstract_view):
    def __init__(self):
        self.sexe_affiche = \
            label_sexe_service.get_all_donnee_from_db()
        self.sexe_nom = [donnee.nom_sexe for donnee in self.sexe_affiche]
#DAO label nom
        choix_donnees = self.sexe_nom
        choix_donnees.append(Separator())
        choix_donnees.append("Retour menu paramétrage")
        self.questions = [
            {
                'type':'list',
                'name':'donnee hospi',
                'message': 'Sur quel sexe souhaitez-vous afficher les résultats',
                'choices': choix_donnees
            }
        ]
    def display_info(self):
        print('Sexe actuel : {}'.format(Abstract_view.session.sexe))

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['donnee hospi'] == "Retour menu paramétrage":
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()  # lance le nouveau menu
        else:
            index = self.sexe_nom.index(resp['donnee hospi'])
            Abstract_view.session.sexe = str(index)
            from view.Parametrages_hospi_view import parametrages_hospi_view
            return parametrages_hospi_view()  # lance le nouveau menu
