from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view


class resume_tests_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Que voulez vous faire ? :',
                'choices': [
                    'Changer les paramètres',
                    Separator(),
                    'Menu Principal'
                ]
            }
        ]

    def display_info(self):
        print('Paramètres actuels :')
        print(Abstract_view.session.indicateurs)
        print('Classe d\'âge : {}'.format(Abstract_view.session.clage))
        print('Sexe : {}'.format(Abstract_view.session.sexe))
        print('Période : {}'.format(Abstract_view.session.periode))
        print('Zone géographique (région) : {}'.format(Abstract_view.session.zonegeoreg))
        print('Zone géographique (département) : {}'.format(Abstract_view.session.zonegeodep))

# ici produire l'affichage de donnée sous forme de tableau affinée où on ne comprare les les infoss

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse["menu"] == 'Changer les paramètres':
            from view.Donnees_hospi_view import Donnees_hospi_view
            return Donnees_hospi_view()
        else:
            from view.start_view import StartView
            return StartView()
