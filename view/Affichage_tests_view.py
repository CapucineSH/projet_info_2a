# -*- coding: ISO-8859-1 -*-
from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view

class affichage_tests_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Que souhaitez-vous faire ?',
                'choices': [
                    'Afficher l\'ensemble des donn�es associ�es aux param�trages',
                    'Afficher un r�sum� des param�tres choisis',
                    Separator(),
                    'Revenir au menu principal',
                ]
            }
        ]

    def display_info(self):
        print('Param�tres actuels :')
        print('Classe d\'�ge : {}'.format(Abstract_view.session.clage))
        print('Sexe : {}'.format(Abstract_view.session.sexe))
        print('P�riode : {}'.format(Abstract_view.session.periode))
        print('Zone g�ographique (r�gion) : {}'.format(Abstract_view.session.zonegeoreg))
        print('Zone g�ographique (d�partement) : {}'.format(Abstract_view.session.zonegeodep))

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Afficher un r�sum� des param�tres choisis':
            # efface le paramtrage
            from view.Resume_tests_view import resume_tests_view
            return resume_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Afficher l\'ensemble des donn�es associ�es aux param�trages':
            from view.Tout_tests_view import tout_tests_view
            return tout_tests_view() # lance le nouveau menu
        else:
            from view.start_view import StartView
            return StartView() # retourne au menu principal
