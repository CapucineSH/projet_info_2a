# -*- coding: ISO-8859-1 -*-
from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view

class parametrages_hospi_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Que souhaitez-vous faire ?',
                'choices': [
                    'S�lectionner une classe d\'�ge',
                    'S�lectionner un sexe',
                    'S�lectionner une date ou une p�riode',
                    'S�lectionner une zone g�ographique',
                    Separator(),
                    'Fin du param�trage, continuer vers le menu d\'affichage',
                    Separator(),
                    'Retourner au choix des donn�es'
                ]
            }
        ]


    def display_info(self):
        print('Param�tres actuels :')
        print(Abstract_view.session.indicateurs)
        print('Classe d\'�ge : {}'.format(Abstract_view.session.clage))
        print('Date : {}'.format(Abstract_view.session.date))
        print('Sexe : {}'.format(Abstract_view.session.sexe))
        print('P�riode : {}'.format(Abstract_view.session.periode))
        print('Zone g�ographique (r�gion) : {}'.format(Abstract_view.session.zonegeoreg))
        print('Zone g�ographique (d�partement) : {}'.format(Abstract_view.session.zonegeodep))
        print('--------------------')
        print(
        'Au terme de votre param�trage (en cliquant sur "afficher les donn�es avec ces param�tres",'
        ' vous pourrez choisir entre une information exhaustive ou un r�sum� d\'information.')

    def make_choice(self):



        resp = prompt(self.questions)
        if resp['menu'] == 'Retourner au choix des donn�es':
            # efface le paramtrage
            Abstract_view.session.indicateurs=[]
            from view.choix_donnees_hospi_view import Choix_donnees_hospi_view
            return Choix_donnees_hospi_view()  # lance le nouveau menu
        if resp['menu'] == 'S�lectionner une classe d\'�ge':
            from view.Param_clage_hospi_view import Param_clage_hospi_view
            return Param_clage_hospi_view() # lance le nouveau menu
        if resp['menu'] == 'S�lectionner un sexe':
            from view.Param_sexe_hospi_view import Param_sexe_hospi_view
            return Param_sexe_hospi_view() # lance le nouveau menu
        if resp['menu'] == 'S�lectionner une date ou une p�riode':
            from view.Param_date_hospi_view import param_date_hospi_view
            return param_date_hospi_view() # lance le nouveau menu
        if resp['menu'] == 'S�lectionner une zone g�ographique':

            from view.Param_zone_hospi_view import Param_zone_hospi_view
            return Param_zone_hospi_view() # lance le nouveau menu
        if resp['menu'] == 'Fin du param�trage, continuer vers le menu d\'affichage':
            from view.Affichage_hospi_view import affichage_hospi_view
            return affichage_hospi_view()





