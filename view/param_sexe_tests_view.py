from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view
from Services.Label_sexe_service import label_sexe_service
from Services.Departements_region_service import departements_region_service

class Param_sexe_tests_view(Abstract_view):
    def __init__(self):
        self.sexe_affiche = \
            label_sexe_service.get_all_donnee_from_db()
        self.sexe_nom = \
            [donnee.nom_sexe for donnee in self.sexe_affiche]
        # DAO label nom
        choix_donnees = self.sexe_nom
        choix_donnees.append(Separator())
        choix_donnees.append("Retour menu paramétrage")
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Sur quel sexe souhaitez-vous afficher les résultats ?',
                'choices': choix_donnees
            }
        ]

    def display_info(self):
        print('Sexe actuel : {}'.format(Abstract_view.session.sexe))

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Retour menu paramétrage':
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Tous':
            Abstract_view.session.sexe = '0'
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Hommes':
            # département et sexe incompatibles : vérification
            if len(Abstract_view.session.zonegeodep) != 1 :
                Abstract_view.session.sexe = '1'
                from view.parametrage_tests_view import Parametrage_tests_view
                return Parametrage_tests_view()
            elif len(Abstract_view.session.zonegeodep) == 1 :
                return Param_sexe_conf_tests_view(1)  # lance le nouveau menu
        if resp['menu'] == 'Femmes':
            # département et sexe incompatibles : vérification
            if len(Abstract_view.session.zonegeodep) != 1 :
                Abstract_view.session.sexe = '2'
                from view.parametrage_tests_view import Parametrage_tests_view
                return Parametrage_tests_view()
            elif len(Abstract_view.session.zonegeodep) == 1 :
                return Param_sexe_conf_tests_view(2)  # lance le nouveau menu
        # on retourne au menu précédent dans tous les cas


class Param_sexe_conf_tests_view(Abstract_view):
    def __init__(self,sexemem):
        self.questions = [
            {
                'type': 'confirm',
                'name': 'menu',
                'message': 'Souhaitez-vous continuer ? Votre paramètre de zone sera rétabli à la France entière.'
            }
        ]
        self.sexemem = sexemem

    def display_info(self):
        print('Les données par sexe ne sont pas disponibles par département. Or la zone sélectionnée est un département.')

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == False:
            return Param_sexe_tests_view()  # lance le nouveau menu
        if resp['menu'] == True:
            Abstract_view.session.sexe = self.sexemem
            zonegeo2_affiche = \
                departements_region_service.get_all_donnee_from_db()
            Abstract_view.session.zonegeodep = [donnee.num_dep for donnee in zonegeo2_affiche]
            from view.parametrage_tests_view import Parametrage_tests_view
            return Parametrage_tests_view()  # lance le nouveau menu