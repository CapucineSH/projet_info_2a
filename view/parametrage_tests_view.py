from PyInquirer import Separator, prompt
from view.abstract_view import Abstract_view

class Parametrage_tests_view(Abstract_view):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Que souhaitez-vous faire ?',
                'choices': [
                    'Sélectionner une classe d\'âge',
                    'Sélectionner un sexe',
                    'Sélectionner une date ou période',
                    'Sélectionner une zone géographique',
                    Separator(),
                    'Fin du paramétrage, continuer vers le menu d\'affichage',
                    Separator(),
                    'Retourner au choix des données'
                ]
            }
        ]

    def display_info(self):
        print('Paramètres actuels :')
        print('Classe d\'âge : {}'.format(Abstract_view.session.clage))
        print('Sexe : {}'.format(Abstract_view.session.sexe))
        print('Période : {}'.format(Abstract_view.session.periode))
        print('Zone géographique (région) : {}'.format(Abstract_view.session.zonegeoreg))
        print('Zone géographique (département) : {}'.format(Abstract_view.session.zonegeodep))
        print('--------------------')
        print('Au terme de votre paramétrage (en cliquant sur "afficher les données avec ces paramètres",'
              ' vous pourrez choisir entre une information exhaustive ou un résumé d\'information')

    def make_choice(self):
        resp = prompt(self.questions)
        if resp['menu'] == 'Retourner au choix des données':
            # efface le paramétrage
            from view.choix_donnees_tests_view import Choix_donnees_tests_view
            return Choix_donnees_tests_view()  # lance le nouveau menu
        if resp['menu'] == 'Sélectionner une classe d\'âge':
            from view.param_clage_tests_view import Param_clage_tests_view
            return Param_clage_tests_view() # lance le nouveau menu
        if resp['menu'] == 'Sélectionner un sexe':
            from view.param_sexe_tests_view import Param_sexe_tests_view
            return Param_sexe_tests_view() # lance le nouveau menu
        if resp['menu'] == 'Sélectionner une date ou période':
            from view.Param_date_tests_view import param_date_tests_view
            return param_date_tests_view()
        if resp['menu'] == 'Sélectionner une zone géographique':
            from view.param_zone_tests_view import Param_zone_tests_view
            return Param_zone_tests_view() # lance le nouveau menu
        if resp['menu'] == 'Fin du paramétrage, continuer vers le menu d\'affichage':
            from view.Affichage_tests_view import affichage_tests_view
            return affichage_tests_view()  # lance le nouveau menu