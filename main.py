# -*- coding: ISO-8859-1 -*-
from view.start_view import StartView


if __name__ == '__main__':
    # on d�marre sur l'�cran accueil
    current_vue = StartView()

    # tant qu'on a un �cran � afficher, on continue
    while current_vue:
        # on affiche une bordure pour s�parer les vue
        with open('assets/border.txt', 'r', encoding="utf-8") as asset:
            print(asset.read())
        # les infos � afficher
        current_vue.display_info()
        # le choix que doit saisir l'utilisateur
        current_vue = current_vue.make_choice()

