import unittest
from covid_api.business_object.Departements_region import departements_region


class Test_Departements_Region(unittest.TestCase):
    def test_set_num_dep(self):
        """Test vérifier que le getter num_dep fonctionne"""
        # GIVEN
        expected_num_dep = "10"
        donnee1 = departements_region(dep_name="pp", Population="0", num_dep=expected_num_dep, region_name="po")
        # WHEN
        # THEN
        self.assertEqual(expected_num_dep, donnee1.num_dep)

    def test_set_dep_name(self):
        """Test vérifier que le setter dep_name fonctionne"""
        # GIVEN
        expected_dep_name = "dep"
        donnee2 = departements_region(dep_name=expected_dep_name, Population="0", num_dep="0", region_name="po")
        # WHEN
        # THEN
        self.assertEqual(expected_dep_name, donnee2.dep_name)

    def test_set_region_name(self):
        """Test vérifier que le setter region_name fonctionne"""
        # GIVEN
        expected_region_name = "reg"
        donnee2 = departements_region(dep_name="dep", Population="0", num_dep="0", region_name=expected_region_name)
        # WHEN
        # THEN
        self.assertEqual(expected_region_name, donnee2.region_name)

    def test_set_population(self):
        """Test vérifier que le setter population fonctionne"""
        # GIVEN
        expected_population = "100"
        donnee3 = departements_region(dep_name="dep", Population=expected_population, num_dep="0", region_name="reg")
        # WHEN
        # THEN
        self.assertEqual(expected_population, donnee3.Population)



if __name__ == '__main__':
    unittest.main()