from unittest import TestCase
from covid_api.dao.Departements_region_dao import departements_region_dao
from covid_api.business_object.Donnees_tests_dep import donnees_tests_dep


from unittest import TestCase
from covid_api.dao.Departements_region_dao import departements_region_dao
from covid_api.business_object.Departements_region import departements_region


class Test_Departements_Region_Dao(TestCase):

    def test_create(self):
        """
        On teste l'insertion en verifiant que l'on a bien un num_dep pour Departement/Region.
        """
        # GIVEN
        nouvelle_donnee = departements_region(
            dep_name="dep"
            , Population=0
            , num_dep="316"
            , region_name=0)

        # WHEN
        nouvelle_donnee_dep = departements_region_dao.create(nouvelle_donnee)

        # THEN
        self.assertIsNotNone(nouvelle_donnee_dep.num_dep)
        departements_region_dao.delete(nouvelle_donnee_dep)


    def test_delete(self):
        """
        On teste la suppression en verifiant que l'on supprime bien la ligne
        que l'on vient d'insérer.
        """
        # GIVEN
        donnee2 = departements_region(
            dep_name="dep"
            , Population=0
            , num_dep="106"
            , region_name="0")

        # WHEN
        donnee2_new = departements_region_dao.create(donnee2)
        deleted = departements_region_dao.delete(donnee2_new)

        # THEN
        self.assertTrue(deleted)

    def test_find_by_num_dep(self):
        # GIVEN
        donnee1 = departements_region(
            dep_name="dep"
            , Population=0
            , num_dep="156"
            , region_name="0")

        # WHEN
        donnee1_new = departements_region_dao.create(donnee1)

        donnee1_db = departements_region_dao.find_by_num_dep(donnee1_new.num_dep)
        # On supprime la ligne
        departements_region_dao.delete(donnee1)


        # THEN
        self.assertIsInstance(donnee1_db, departements_region,"n'est pas de la bonne classe")
        self.assertEqual(donnee1_new.num_dep, donnee1_db.num_dep,"problemèe dans les id")
        self.assertEqual(donnee1_new.region_name, donnee1_db.region_name,"problemèe dans les noms de region")

    def test_find_by_region_name(self):
        # GIVEN
        donnee4 = departements_region(
            dep_name="dep"
            , Population=0
            , num_dep="978"
            , region_name="0")

        # WHEN
        donnee4_new = departements_region_dao.create(donnee4)

        donnee4_db = departements_region_dao.find_by_region_name(donnee4_new.region_name)
        # On supprime la ligne
        departements_region_dao.delete(donnee4)

        # THEN
        self.assertIsInstance(donnee4_db, departements_region, "n'est pas de la bonne classe")
        self.assertEqual(donnee4_new.region_name, donnee4_db.region_name, "problemèe dans les nom de region")

    def test_find_by_dep_name(self):
        # GIVEN
        donnee6 = departements_region(
            dep_name="dep"
            , Population=0
            , num_dep="256"
            , region_name="0")

        # WHEN
        donnee6_new = departements_region_dao.create(donnee6)

        donnee6_db = departements_region_dao.find_by_dep_name(donnee6_new.dep_name)
        # On supprime la ligne
        departements_region_dao.delete(donnee6)


        # THEN
        self.assertIsInstance(donnee6_db, departements_region,"n'est pas de la bonne classe")
        self.assertEqual(donnee6_new.dep_name, donnee6_db.dep_name,"problemèe dans les nom de departements")

    def test_find_by_Population(self):
        # GIVEN
        donnee5 = departements_region(
            dep_name="dep"
            , Population="0"
            , num_dep="826"
            , region_name="0")

        # WHEN
        donnee5_new = departements_region_dao.create(donnee5)

        donnee5_db = departements_region_dao.find_by_Population(donnee5_new.Population)
        # On supprime la ligne
        departements_region_dao.delete(donnee5)

        # THEN
        self.assertIsInstance(donnee5_db, departements_region, "n'est pas de la bonne classe")
        self.assertEqual(donnee5_new.Population, donnee5_db.Population, "problemèe dans les populations")

    def test_find_all(self):
        # GIVEN

        # WHEN
        donnee3 = departements_region_dao.find_all()
        # THEN
        self.assertIsNotNone(donnee3)
